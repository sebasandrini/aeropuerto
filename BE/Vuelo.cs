﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BE
{
    public class Vuelo
    {
        private int nroVuelo;

        public int NroVuelo
        {
            get { return nroVuelo; }
            set { nroVuelo = value; }
        }

        private DateTime fechaPartida;

        public DateTime FechaPartida
        {
            get { return fechaPartida; }
            set { fechaPartida = value; }
        }

        private DateTime fechaLlegada;

        public DateTime FechaLlegada
        {
            get { return fechaLlegada; }
            set { fechaLlegada = value;}
        }

        private Destino origen;

        public Destino Origen
        {
            get { return origen; }
            set { origen = value; }
        }

        private Destino destino;

        public Destino Destino
        {
            get { return destino; }
            set { destino = value; }
        }

        private Avion avion;

        public Avion Avion
        {
            get { return avion; }
            set { avion = value; }
        }

        private List<Destino> escalas = new List<Destino>();

        public List<Destino> Escalas
        {
            get { return escalas; }
            set { escalas = value; }
        }

        private string tipo;

        public string Tipo
        {
            get { return tipo; }
            set { tipo = value; }
        }

        private Enum_EstadoVuelo estadoVuelo;

        public Enum_EstadoVuelo EstadoVuelo
        {
            get { return estadoVuelo; }
            set { estadoVuelo = value; }
        }

        private string ocupacion;

        public string Ocupacion
        {
            get { return ocupacion; }
            set { ocupacion = value; }
        }

        private List<Pasajero> pasajeros = new List<Pasajero>();

        public List<Pasajero> Pasajeros
        {
            get { return pasajeros; }
            set { pasajeros = value; }
        }



    }
}