﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BE
{
    public class Avion
    {
        private int id;

        public int Id
        {
            get { return id; }
            set { id = value; }
        }

        private string tipo;

        public string Tipo
        {
            get { return tipo; }
            set { tipo = value; }
        }

        private int capacidad;

        public int Capacidad
        {
            get { return capacidad; }
            set { capacidad = value; }
        }

        private int activo;

        public int Activo
        {
            get { return activo; }
            set { activo = value; }
        }

        public override string ToString()
        {
            return tipo + " - Capacidad: " + capacidad.ToString() + " personas.";
        }

    }
}