﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BE
{
    public class Pasaje
    {
        private int id;

        public int Id
        {
            get { return id; }
            set { id = value; }
        }

        private Pasajero pasajero;

        public Pasajero Pasajero
        {
            get { return pasajero; }
            set { pasajero = value; }
        }

        private Vuelo vuelo;

        public Vuelo Vuelo
        {
            get { return vuelo; }
            set { vuelo = value; }
        }

        private DateTime fechaCompra;

        public DateTime FechaCompra
        {
            get { return fechaCompra; }
            set { fechaCompra = value; }
        }

        private double total;

        public double Total
        {
            get { return total; }
            set { total = value; }
        }

    }
}