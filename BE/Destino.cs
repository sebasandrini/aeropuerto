﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BE
{
    public class Destino
    {
        private int id;

        public int Id
        {
            get { return id; }
            set { id = value; }
        }

        private string ciudad;

        public string Ciudad
        {
            get { return ciudad; }
            set { ciudad = value; }
        }

        private string pais;

        public string Pais
        {
            get { return pais; }
            set { pais = value; }
        }

        private string aeropuerto;

        public string Aeropuerto
        {
            get { return aeropuerto; }
            set { aeropuerto = value; }
        }

        private string latitud;

        public string Latitud
        {
            get { return latitud; }
            set { latitud = value; }
        }

        private string longitud;

        public string Longitud
        {
            get { return longitud; }
            set { longitud = value; }
        }

        private int activo;

        public int Activo
        {
            get { return activo; }
            set { activo = value; }
        }

        public override string ToString()
        {
            return ciudad + ", " + pais;
        }
    }
}