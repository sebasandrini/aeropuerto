﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BE
{
    public class Evento
    {
        private int id;

        public int ID
        {
            get { return id; }
            set { id = value; }
        }

        private DateTime fechaEvento;

        public DateTime FechaEvento
        {
            get { return fechaEvento; }
            set { fechaEvento = value; }
        }

        private Vuelo vuelo;

        public Vuelo Vuelo
        {
            get { return vuelo; }
            set { vuelo = value; }
        }

        private string tipoEvento;

        public string TipoEvento
        {
            get { return tipoEvento; }
            set { tipoEvento = value; }
        }

    }
}