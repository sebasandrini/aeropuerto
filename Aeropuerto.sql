USE [master]
GO
/****** Object:  Database [Aeropuerto]    Script Date: 17/11/2020 23:56:09 ******/
CREATE DATABASE [Aeropuerto]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'Aeropuerto', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL15.SQLEXPRESS\MSSQL\DATA\Aeropuerto.mdf' , SIZE = 8192KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'Aeropuerto_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL15.SQLEXPRESS\MSSQL\DATA\Aeropuerto_log.ldf' , SIZE = 8192KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
 WITH CATALOG_COLLATION = DATABASE_DEFAULT
GO
ALTER DATABASE [Aeropuerto] SET COMPATIBILITY_LEVEL = 150
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [Aeropuerto].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [Aeropuerto] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [Aeropuerto] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [Aeropuerto] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [Aeropuerto] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [Aeropuerto] SET ARITHABORT OFF 
GO
ALTER DATABASE [Aeropuerto] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [Aeropuerto] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [Aeropuerto] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [Aeropuerto] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [Aeropuerto] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [Aeropuerto] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [Aeropuerto] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [Aeropuerto] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [Aeropuerto] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [Aeropuerto] SET  DISABLE_BROKER 
GO
ALTER DATABASE [Aeropuerto] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [Aeropuerto] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [Aeropuerto] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [Aeropuerto] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [Aeropuerto] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [Aeropuerto] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [Aeropuerto] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [Aeropuerto] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [Aeropuerto] SET  MULTI_USER 
GO
ALTER DATABASE [Aeropuerto] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [Aeropuerto] SET DB_CHAINING OFF 
GO
ALTER DATABASE [Aeropuerto] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [Aeropuerto] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [Aeropuerto] SET DELAYED_DURABILITY = DISABLED 
GO
ALTER DATABASE [Aeropuerto] SET QUERY_STORE = OFF
GO
USE [Aeropuerto]
GO
/****** Object:  Table [dbo].[Avion]    Script Date: 17/11/2020 23:56:09 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Avion](
	[id] [int] NOT NULL,
	[tipo] [varchar](50) NULL,
	[capacidad] [int] NULL,
	[activo] [int] NULL,
 CONSTRAINT [PK_Avion] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Destino]    Script Date: 17/11/2020 23:56:09 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Destino](
	[id] [int] NOT NULL,
	[ciudad] [varchar](50) NULL,
	[pais] [varchar](50) NULL,
	[aeropuerto] [varchar](50) NULL,
	[latitud] [varchar](50) NULL,
	[longitud] [varchar](50) NULL,
	[activo] [int] NULL,
 CONSTRAINT [PK_Destino] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Evento]    Script Date: 17/11/2020 23:56:09 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Evento](
	[id] [int] NOT NULL,
	[fechaEvento] [datetime] NULL,
	[nroVuelo] [int] NULL,
	[tipoEvento] [varchar](50) NULL,
 CONSTRAINT [PK_Evento] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Pasaje]    Script Date: 17/11/2020 23:56:09 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Pasaje](
	[id] [int] NOT NULL,
	[pasajero] [int] NULL,
	[fechaCompra] [datetime] NULL,
	[nroVuelo] [int] NULL,
	[total] [money] NULL,
 CONSTRAINT [PK_Pasaje] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Pasajero]    Script Date: 17/11/2020 23:56:09 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Pasajero](
	[id] [int] NOT NULL,
	[nombre] [varchar](50) NULL,
	[apellido] [varchar](50) NULL,
	[telefono] [int] NULL,
	[mail] [varchar](50) NULL,
	[dni] [int] NULL,
	[activo] [int] NULL,
 CONSTRAINT [PK_Pasajero] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Vuelo]    Script Date: 17/11/2020 23:56:09 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Vuelo](
	[nroVuelo] [int] NOT NULL,
	[fechaPartida] [datetime] NULL,
	[fechaLlegada] [datetime] NULL,
	[origen] [int] NULL,
	[destino] [int] NULL,
	[avion] [int] NULL,
	[escalas] [varchar](50) NULL,
	[estadoVuelo] [int] NULL,
	[tipo] [varchar](50) NULL,
	[ocupacion] [int] NULL,
 CONSTRAINT [PK_Vuelo] PRIMARY KEY CLUSTERED 
(
	[nroVuelo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  StoredProcedure [dbo].[CancelarVuelo]    Script Date: 17/11/2020 23:56:09 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[CancelarVuelo]
@nroVuelo int
as
begin

	update Vuelo set
	estadoVuelo = 3
	where nroVuelo = @nroVuelo

end
GO
/****** Object:  StoredProcedure [dbo].[EliminarAvion]    Script Date: 17/11/2020 23:56:09 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[EliminarAvion]
@id int
as
begin

	update Avion set
	activo = 0
	where id =  @id

end
GO
/****** Object:  StoredProcedure [dbo].[EliminarDestino]    Script Date: 17/11/2020 23:56:09 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[EliminarDestino]
@id int
as
begin

	update Destino set
		activo = 0
		where id = @id

end
GO
/****** Object:  StoredProcedure [dbo].[EliminarPasajero]    Script Date: 17/11/2020 23:56:09 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[EliminarPasajero]
@id int
as
begin

	update Pasajero set
	activo = 0
	where id = @id

end
GO
/****** Object:  StoredProcedure [dbo].[InsertarAvion]    Script Date: 17/11/2020 23:56:09 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[InsertarAvion]
@tipo varchar(50), @capacidad int
as
begin
	
	declare @id int
	declare @activo int
	set @id = (select ISNULL( MAX(a.id) , 0) + 1 from Avion a)
	set @activo = 1

	insert into Avion (id,tipo,capacidad,activo) values (@id, @tipo,@capacidad,@activo)

end
GO
/****** Object:  StoredProcedure [dbo].[InsertarDestino]    Script Date: 17/11/2020 23:56:09 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[InsertarDestino]
@ciudad varchar(50),@pais varchar(50),@aeropuerto varchar(50),@latitud varchar(50),@longitud varchar(50)
as
begin

	declare @id int
	declare @activo int
	set @id = (select ISNULL( MAX(d.id) , 0) + 1 from Destino d)
	set @activo = 1

	insert into Destino (id,ciudad,pais,aeropuerto,latitud,longitud,activo) values (@id, @ciudad,@pais,@aeropuerto,@latitud,@longitud,@activo)

end
GO
/****** Object:  StoredProcedure [dbo].[InsertarEvento]    Script Date: 17/11/2020 23:56:09 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[InsertarEvento]
@fechaEvento varchar(500), @nroVuelo int, @tipoEvento varchar(50), @estadoVuelo int
as
begin

	declare @id int
	declare @fechaEv datetime
	set @id = (select ISNULL( MAX(e.id) , 0) + 1 from Evento e)
	set @fechaEv = (select convert(varchar, @fechaEvento, 113))

	insert into Evento (id, fechaEvento, nroVuelo, tipoEvento) values (@id, @fechaEv, @nroVuelo, @tipoEvento)

	update Vuelo set
	estadoVuelo = @estadoVuelo
	where nroVuelo = @nroVuelo

end
GO
/****** Object:  StoredProcedure [dbo].[InsertarPasaje]    Script Date: 17/11/2020 23:56:09 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[InsertarPasaje]
@pasajero int, @nroVuelo int, @total money
as
begin

	declare @id int
	declare @fechaCompra datetime
	set @id = (select ISNULL( MAX(p.id) , 0) + 1 from Pasaje p)
	set @fechaCompra = GETDATE()

	insert into Pasaje (id, pasajero,fechaCompra,nroVuelo,total) values (@id,@pasajero,@fechaCompra,@nroVuelo,@total)

	update Vuelo set
	ocupacion = ocupacion +1
	where nroVuelo = @nroVuelo

end 


update Vuelo set
fechaLlegada = '2020-11-17 16:00:00.000'
where nroVuelo = 1010



select * from Pasaje
GO
/****** Object:  StoredProcedure [dbo].[InsertarPasajero]    Script Date: 17/11/2020 23:56:09 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[InsertarPasajero]
@nombre varchar(50), @apellido varchar(50), @dni int, @mail varchar(50), @telefono int
as
begin
	
	declare @id int
	declare @activo int
	set @id = (select ISNULL( MAX(p.id) , 0) + 1 from Pasajero p)
	set @activo = 1

	insert into Pasajero (id,nombre,apellido,dni,mail,telefono,activo) values (@id, @nombre,@apellido,@dni,@mail,@telefono,@activo)

end
GO
/****** Object:  StoredProcedure [dbo].[InsertarVuelo]    Script Date: 17/11/2020 23:56:09 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[InsertarVuelo]
@fechaPartida varchar(500), @fechaLlegada varchar(500), @origen int, @destino int, @avion int, @escalas varchar(50), @tipo varchar(50)
as
begin
	
	declare @nroVuelo int
	declare @estadoVuelo int
	declare @ocupacion int
	declare @partida datetime
	declare @llegada datetime
	set @nroVuelo = (select ISNULL( MAX(v.nroVuelo) , 1000) + 1 from Vuelo v)
	set @estadoVuelo = 0
	set @partida = (select convert(varchar, @fechaPartida, 113))
	set @llegada = (select convert(varchar, @fechaLlegada, 113))
	set @ocupacion = 0

	insert into Vuelo (nroVuelo,fechaPartida,fechaLlegada,origen,destino,avion,escalas, estadoVuelo, tipo, ocupacion) values (@nroVuelo, @partida,@llegada,@origen,@destino,@avion,@escalas,@estadoVuelo,@tipo,@ocupacion)

end
GO
/****** Object:  StoredProcedure [dbo].[ListarAviones]    Script Date: 17/11/2020 23:56:09 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[ListarAviones]
as
begin

	select a.id, a.tipo, a.capacidad, a.activo from Avion a

end
GO
/****** Object:  StoredProcedure [dbo].[ListarDestinos]    Script Date: 17/11/2020 23:56:09 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[ListarDestinos]
as
begin

	select d.id, d.ciudad, d.pais,d.aeropuerto,d.latitud,d.longitud,d.activo from Destino d

end
GO
/****** Object:  StoredProcedure [dbo].[ListarEventos]    Script Date: 17/11/2020 23:56:09 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[ListarEventos]
as
begin

	select e.id, e.fechaEvento, e.nroVuelo, e.tipoEvento from Evento e

end
GO
/****** Object:  StoredProcedure [dbo].[ListarPasajeros]    Script Date: 17/11/2020 23:56:09 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[ListarPasajeros]
as
begin

	select p.id,p.nombre, p.apellido, p.dni,p.mail,p.telefono, p.activo from Pasajero p

end
GO
/****** Object:  StoredProcedure [dbo].[ListarPasajes]    Script Date: 17/11/2020 23:56:09 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[ListarPasajes]
as
begin

	select p.id, p.pasajero, p.fechaCompra, p.nroVuelo, p.total from Pasaje p

end
GO
/****** Object:  StoredProcedure [dbo].[ListarVuelos]    Script Date: 17/11/2020 23:56:09 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[ListarVuelos]
as
begin

	select v.nroVuelo, v.origen, v.destino, v.fechaPartida, v.fechaLlegada, v.avion, v.escalas, v.estadoVuelo, v.tipo, v.ocupacion from Vuelo v

end
GO
/****** Object:  StoredProcedure [dbo].[ModificarAvion]    Script Date: 17/11/2020 23:56:09 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[ModificarAvion]
@id int, @tipo varchar(50), @capacidad int
as
begin

	update Avion set
	tipo = @tipo,
	capacidad = @capacidad
	where id = @id

end
GO
/****** Object:  StoredProcedure [dbo].[ModificarDestino]    Script Date: 17/11/2020 23:56:09 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[ModificarDestino]
@id int,@ciudad varchar(50),@pais varchar(50),@aeropuerto varchar(50),@latitud varchar(50),@longitud varchar(50)
as
begin

	update Destino set
		ciudad = @ciudad,
		pais = @pais,
		aeropuerto = @aeropuerto,
		latitud = @latitud,
		longitud = @longitud
		where id = @id

end
GO
/****** Object:  StoredProcedure [dbo].[ModificarPasajero]    Script Date: 17/11/2020 23:56:09 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[ModificarPasajero]
@id int, @nombre varchar(50), @apellido varchar(50), @dni int, @mail varchar(50), @telefono int
as
begin

	update Pasajero set
	nombre = @nombre,
	apellido = @apellido,
	dni = @dni,
	mail = @mail,
	telefono = @telefono
	where id = @id

end
GO
USE [master]
GO
ALTER DATABASE [Aeropuerto] SET  READ_WRITE 
GO
