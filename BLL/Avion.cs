﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BLL
{
    public class Avion
    {
        DAL.MP_Avion mp_avion = new DAL.MP_Avion();

        public int InsertarAvion(BE.Avion avion)
        {
            int resultado = mp_avion.InsertarAvion(avion);
            return resultado;
        }

        public int ModificarAvion(BE.Avion avion)
        {
            int resultado = mp_avion.ModificarAvion(avion);
            return resultado;
        }

        public int EliminarAvion(BE.Avion avion)
        {
            int resultado = mp_avion.EliminarAvion(avion);
            return resultado;
        }
        public List<BE.Avion> ListarAviones()
        {
            return mp_avion.ListarAviones();
        }
    }
}