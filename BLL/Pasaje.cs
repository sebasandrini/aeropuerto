﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BLL
{
    public class Pasaje
    {
        DAL.MP_Pasaje mp_pasaje = new DAL.MP_Pasaje();

        public int InsertarPasaje(BE.Pasaje pasaje)
        {
            int resultado = mp_pasaje.InsertarPasaje(pasaje);
            return resultado;
        }

        public List<BE.Pasaje> ListarPasajes()
        {
            return mp_pasaje.ListarPasajes();
        }
    }
}