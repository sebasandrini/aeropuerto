﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BLL
{
    public class Pasajero
    {
        DAL.MP_Pasajero mp_pax = new DAL.MP_Pasajero();

        public int InsertarPasajero(BE.Pasajero pasajero)
        {
            int resultado = mp_pax.InsertarPasajero(pasajero);
            return resultado;
        }

        public int ModificarPasajero(BE.Pasajero pasajero)
        {
            int resultado = mp_pax.ModificarPasajero(pasajero);
            return resultado;
        }

        public int EliminarPasajero(BE.Pasajero pasajero)
        {
            int resultado = mp_pax.EliminarPasajero(pasajero);
            return resultado;
        }
        public List<BE.Pasajero> ListarPasajeros()
        {
            return mp_pax.ListarPasajeros();
        }
    }
}