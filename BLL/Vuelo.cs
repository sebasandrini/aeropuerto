﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BLL
{
    public class Vuelo
    {
        DAL.MP_Vuelo mp_vuelo = new DAL.MP_Vuelo();

        public int InsertarVuelo(BE.Vuelo vuelo)
        {
            int resultado = mp_vuelo.InsertarVuelo(vuelo);
            return resultado;
        }

        public int CancelarVuelo(BE.Vuelo vuelo)
        {
            int resultado = mp_vuelo.CancelarVuelo(vuelo);
            return resultado;
        }

        public List<BE.Vuelo> ListarVuelos()
        {
            return mp_vuelo.ListarVuelos();
        }
    }
}