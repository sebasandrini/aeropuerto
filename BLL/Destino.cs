﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BLL
{
    public class Destino
    {
        DAL.MP_Destino mp_destino = new DAL.MP_Destino();

        public int InsertarDestino(BE.Destino destino)
        {
            int resultado = mp_destino.InsertarDestino(destino);
            return resultado;
        }

        public int ModificarDestino(BE.Destino destino)
        {
            int resultado = mp_destino.ModificarDestino(destino);
            return resultado;
        }

        public int EliminarDestino(BE.Destino destino)
        {
            int resultado = mp_destino.EliminarDestino(destino);
            return resultado;
        }
        public List<BE.Destino> ListarDestinos()
        {
            return mp_destino.ListarDestinos();
        }
    }
}