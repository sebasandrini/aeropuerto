﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BLL
{
    public class Evento
    {
        DAL.MP_Evento mp_evento = new DAL.MP_Evento();

        public int InsertarEvento(BE.Evento evento)
        {
            int resultado = mp_evento.InsertarEvento(evento);
            return resultado;
        }

        public List<BE.Evento> ListarEventos()
        {
            return mp_evento.ListarEventos();
        }
    }
}