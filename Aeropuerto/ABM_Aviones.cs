﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Aeropuerto
{
    public partial class ABM_Aviones : Form
    {
        public ABM_Aviones()
        {
            InitializeComponent();
        }

        BLL.Avion gestor_avion = new BLL.Avion();
        BE.Avion avion = null;

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void rdAlta_CheckedChanged(object sender, EventArgs e)
        {
            lblTipo.Visible = true;
            lblCapacidad.Visible = true;
            txtCapacidad.Visible = true;
            cmbTipo.Visible = true;
            btnAlta.Text = "Registrar Avión";
            btnAlta.Visible = true;
            btnCancelar.Visible = true;
            dgvAviones.Visible = true;
        }

        private void rdModificar_CheckedChanged(object sender, EventArgs e)
        {
            lblTipo.Visible = true;
            lblCapacidad.Visible = true;
            txtCapacidad.Visible = true;
            cmbTipo.Visible = true;
            btnAlta.Text = "Modificar Avión";
            btnAlta.Visible = true;
            btnCancelar.Visible = true;
            dgvAviones.Visible = true;
        }

        private void rdBaja_CheckedChanged(object sender, EventArgs e)
        {
            lblTipo.Visible = false;
            lblCapacidad.Visible = false;
            txtCapacidad.Visible = false;
            cmbTipo.Visible = false;
            btnAlta.Text = "Eliminar Avión";
            btnAlta.Visible = true;
            btnCancelar.Visible = true;
            dgvAviones.Visible = true;
        }
    
        bool ValidarCampos()
        {
            bool resultado = false;

            if (cmbTipo.Text != "" && !string.IsNullOrWhiteSpace(txtCapacidad.Text))
            {
                if(ValidarNumericos(txtCapacidad.Text))
                {
                    resultado = true;
                }
                else
                {
                    MessageBox.Show("Verifique los campos Capacidad, asientos en primera y asientos económica. Deben ser numéricos.");
                }
            }
            else
            {
                MessageBox.Show("Por favor, complete todos los campos");
            }

                return resultado;
        }
        void LimpiarCampos()
        {
            txtCapacidad.Clear();
            cmbTipo.ResetText();
            lblCapacidad.Visible = false;
            lblTipo.Visible = false;
            txtCapacidad.Visible = false;
            btnAlta.Visible = false;
            btnCancelar.Visible = false;
            dgvAviones.Visible = false;
            rdAlta.Checked = false;
            rdModificar.Checked = false;
            rdBaja.Checked = false;
        }

        void Enlazar()
        {
            dgvAviones.DataSource = null;
            dgvAviones.DataSource = gestor_avion.ListarAviones();
        }
        public bool ValidarNumericos(string valor)
        {
            int i = 0;

            bool resultado = false;

            if (int.TryParse(valor, out i))
            {
                return true;
            }

            return resultado;
        }

        private void dgvAviones_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (rdModificar.Checked)
            {
                BE.Avion avion = (BE.Avion)dgvAviones.SelectedRows[0].DataBoundItem;

                cmbTipo.Text = avion.Tipo;
                txtCapacidad.Text = avion.Capacidad.ToString();
            }
        }

        private void ABM_Aviones_Load(object sender, EventArgs e)
        {
            Enlazar();
        }

        private void btnAlta_Click(object sender, EventArgs e)
        {
            if (rdAlta.Checked)
            {
                if (ValidarCampos())
                {
                    avion = new BE.Avion();
                    avion.Tipo = cmbTipo.Text;
                    avion.Capacidad = int.Parse(txtCapacidad.Text);

                    int resultado = gestor_avion.InsertarAvion(avion);

                    if (resultado != 1)
                    {
                        MessageBox.Show("Problemas en la escritura");
                    }
                    else
                    {
                        MessageBox.Show("Avion registrado exitosamente");
                        Enlazar();
                        LimpiarCampos();
                    }
                }
            }
            else if (rdModificar.Checked)
            {
                if (ValidarCampos())
                {
                    avion = (BE.Avion)dgvAviones.SelectedRows[0].DataBoundItem;
                    avion.Tipo = cmbTipo.Text;
                    avion.Capacidad = int.Parse(txtCapacidad.Text);

                    int resultado = gestor_avion.ModificarAvion(avion);

                    if (resultado != 1)
                    {
                        MessageBox.Show("Problemas en la escritura");
                    }
                    else
                    {
                        MessageBox.Show("Avion modificado exitosamente");
                        LimpiarCampos();
                        Enlazar();
                    }
                }

            }
            else
            {
                if ((BE.Avion)dgvAviones.Rows[0].DataBoundItem != null)
                {
                    BE.Avion avion = (BE.Avion)dgvAviones.SelectedRows[0].DataBoundItem;

                    int resultado = gestor_avion.EliminarAvion(avion);

                    if (resultado != 1)
                    {
                        MessageBox.Show("Problemas en la escritura");
                    }
                    else
                    {
                        MessageBox.Show("Avion eliminado exitosamente");
                        LimpiarCampos();
                        Enlazar();
                    }
                }
                else
                {
                    MessageBox.Show("Debe seleccionar un avion para eliminar");
                }

            }
        }
    }
}
