﻿namespace Aeropuerto
{
    partial class Despegues_Arribos
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.lblFecha = new System.Windows.Forms.Label();
            this.rdDespegue = new System.Windows.Forms.RadioButton();
            this.rdArribo = new System.Windows.Forms.RadioButton();
            this.dtFechaEvento = new System.Windows.Forms.DateTimePicker();
            this.lblVuelos = new System.Windows.Forms.Label();
            this.dgvVuelos = new System.Windows.Forms.DataGridView();
            this.btnRegistrarEvento = new System.Windows.Forms.Button();
            this.btnCancelar = new System.Windows.Forms.Button();
            this.btnBuscar = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dgvVuelos)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(246, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(305, 25);
            this.label1.TabIndex = 0;
            this.label1.Text = "Registrar Despegues y Arribos";
            // 
            // lblFecha
            // 
            this.lblFecha.AutoSize = true;
            this.lblFecha.Location = new System.Drawing.Point(21, 115);
            this.lblFecha.Name = "lblFecha";
            this.lblFecha.Size = new System.Drawing.Size(115, 17);
            this.lblFecha.TabIndex = 1;
            this.lblFecha.Text = "Fecha de Evento";
            this.lblFecha.Visible = false;
            // 
            // rdDespegue
            // 
            this.rdDespegue.AutoSize = true;
            this.rdDespegue.Location = new System.Drawing.Point(251, 66);
            this.rdDespegue.Name = "rdDespegue";
            this.rdDespegue.Size = new System.Drawing.Size(156, 21);
            this.rdDespegue.TabIndex = 2;
            this.rdDespegue.Text = "Registrar Despegue";
            this.rdDespegue.UseVisualStyleBackColor = true;
            this.rdDespegue.CheckedChanged += new System.EventHandler(this.rdDespegue_CheckedChanged);
            // 
            // rdArribo
            // 
            this.rdArribo.AutoSize = true;
            this.rdArribo.Location = new System.Drawing.Point(476, 66);
            this.rdArribo.Name = "rdArribo";
            this.rdArribo.Size = new System.Drawing.Size(129, 21);
            this.rdArribo.TabIndex = 3;
            this.rdArribo.Text = "Registrar Arribo";
            this.rdArribo.UseVisualStyleBackColor = true;
            this.rdArribo.CheckedChanged += new System.EventHandler(this.rdArribo_CheckedChanged);
            // 
            // dtFechaEvento
            // 
            this.dtFechaEvento.CustomFormat = "dd/MM/yyyy HH:mm";
            this.dtFechaEvento.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtFechaEvento.Location = new System.Drawing.Point(172, 115);
            this.dtFechaEvento.Name = "dtFechaEvento";
            this.dtFechaEvento.Size = new System.Drawing.Size(200, 22);
            this.dtFechaEvento.TabIndex = 4;
            this.dtFechaEvento.Visible = false;
            // 
            // lblVuelos
            // 
            this.lblVuelos.AutoSize = true;
            this.lblVuelos.Location = new System.Drawing.Point(12, 168);
            this.lblVuelos.Name = "lblVuelos";
            this.lblVuelos.Size = new System.Drawing.Size(122, 17);
            this.lblVuelos.TabIndex = 5;
            this.lblVuelos.Text = "Seleccionar Vuelo";
            this.lblVuelos.Visible = false;
            // 
            // dgvVuelos
            // 
            this.dgvVuelos.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgvVuelos.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvVuelos.Location = new System.Drawing.Point(11, 198);
            this.dgvVuelos.Name = "dgvVuelos";
            this.dgvVuelos.RowHeadersWidth = 51;
            this.dgvVuelos.RowTemplate.Height = 24;
            this.dgvVuelos.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvVuelos.Size = new System.Drawing.Size(919, 208);
            this.dgvVuelos.TabIndex = 6;
            this.dgvVuelos.Visible = false;
            // 
            // btnRegistrarEvento
            // 
            this.btnRegistrarEvento.Location = new System.Drawing.Point(333, 436);
            this.btnRegistrarEvento.Name = "btnRegistrarEvento";
            this.btnRegistrarEvento.Size = new System.Drawing.Size(136, 45);
            this.btnRegistrarEvento.TabIndex = 7;
            this.btnRegistrarEvento.Text = "Registrar Evento";
            this.btnRegistrarEvento.UseVisualStyleBackColor = false;
            this.btnRegistrarEvento.Visible = false;
            this.btnRegistrarEvento.Click += new System.EventHandler(this.btnRegistrarEvento_Click);
            // 
            // btnCancelar
            // 
            this.btnCancelar.Location = new System.Drawing.Point(506, 436);
            this.btnCancelar.Name = "btnCancelar";
            this.btnCancelar.Size = new System.Drawing.Size(136, 45);
            this.btnCancelar.TabIndex = 8;
            this.btnCancelar.Text = "Cancelar";
            this.btnCancelar.UseVisualStyleBackColor = false;
            this.btnCancelar.Visible = false;
            this.btnCancelar.Click += new System.EventHandler(this.btnCancelar_Click);
            // 
            // btnBuscar
            // 
            this.btnBuscar.Location = new System.Drawing.Point(667, 105);
            this.btnBuscar.Name = "btnBuscar";
            this.btnBuscar.Size = new System.Drawing.Size(151, 37);
            this.btnBuscar.TabIndex = 9;
            this.btnBuscar.Text = "Buscar vuelos";
            this.btnBuscar.UseVisualStyleBackColor = true;
            this.btnBuscar.Visible = false;
            this.btnBuscar.Click += new System.EventHandler(this.btnBuscar_Click);
            // 
            // Despegues_Arribos
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(939, 493);
            this.Controls.Add(this.btnBuscar);
            this.Controls.Add(this.btnCancelar);
            this.Controls.Add(this.btnRegistrarEvento);
            this.Controls.Add(this.dgvVuelos);
            this.Controls.Add(this.lblVuelos);
            this.Controls.Add(this.dtFechaEvento);
            this.Controls.Add(this.rdArribo);
            this.Controls.Add(this.rdDespegue);
            this.Controls.Add(this.lblFecha);
            this.Controls.Add(this.label1);
            this.Name = "Despegues_Arribos";
            this.Text = "Registrar Despegues y Arribos";
            this.Load += new System.EventHandler(this.Despegues_Arribos_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgvVuelos)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lblFecha;
        private System.Windows.Forms.RadioButton rdDespegue;
        private System.Windows.Forms.RadioButton rdArribo;
        private System.Windows.Forms.DateTimePicker dtFechaEvento;
        private System.Windows.Forms.Label lblVuelos;
        private System.Windows.Forms.DataGridView dgvVuelos;
        private System.Windows.Forms.Button btnRegistrarEvento;
        private System.Windows.Forms.Button btnCancelar;
        private System.Windows.Forms.Button btnBuscar;
    }
}