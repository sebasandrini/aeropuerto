﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Aeropuerto
{
    public partial class ABM_Pasajeros : Form
    {
        public ABM_Pasajeros()
        {
            InitializeComponent();
        }

        BLL.Pasajero gestor_pasajero = new BLL.Pasajero();
        BE.Pasajero pasajero = null;

        private void rdAlta_CheckedChanged(object sender, EventArgs e)
        {
            lblNombre.Visible = true;
            lblApellido.Visible = true;
            lblDNI.Visible = true;
            lblMail.Visible = true;
            lblTelefono.Visible = true;
            txtNombre.Visible = true;
            txtApellido.Visible = true;
            txtDNI.Visible = true;
            txtMail.Visible = true;
            txtTelefono.Visible = true;
            btnAlta.Text = "Registrar Pasajero";
            btnAlta.Visible = true;
            btnCancelar.Visible = true;
            dgvPasajeros.Visible = true;
        }

        private void rdModificar_CheckedChanged(object sender, EventArgs e)
        {
            lblNombre.Visible = true;
            lblApellido.Visible = true;
            lblDNI.Visible = true;
            lblMail.Visible = true;
            lblTelefono.Visible = true;
            txtNombre.Visible = true;
            txtApellido.Visible = true;
            txtDNI.Visible = true;
            txtMail.Visible = true;
            txtTelefono.Visible = true;
            btnAlta.Text = "Modificar Pasajero";
            btnAlta.Visible = true;
            btnCancelar.Visible = true;
            dgvPasajeros.Visible = true;
        }

        private void rdBaja_CheckedChanged(object sender, EventArgs e)
        {
            lblNombre.Visible = false;
            lblApellido.Visible = false;
            lblDNI.Visible = false;
            lblMail.Visible = false;
            lblTelefono.Visible = false;
            txtNombre.Visible = false;
            txtApellido.Visible = false;
            txtDNI.Visible = false;
            txtMail.Visible = false;
            txtTelefono.Visible = false;
            btnAlta.Text = "Eliminar Pasajero";
            btnAlta.Visible = true;
            btnCancelar.Visible = true;
            dgvPasajeros.Visible = true;
        }
    
        void Enlazar()
        {
            dgvPasajeros.DataSource = null;
            dgvPasajeros.DataSource = gestor_pasajero.ListarPasajeros();
        }

        bool ValidarCampos()
        {
            bool resultado = false;

            if (!string.IsNullOrWhiteSpace(txtNombre.Text) && !string.IsNullOrWhiteSpace(txtApellido.Text) && !string.IsNullOrWhiteSpace(txtDNI.Text) && !string.IsNullOrWhiteSpace(txtMail.Text) && !string.IsNullOrWhiteSpace(txtTelefono.Text))
            {
                if (ValidarDNI(txtDNI.Text))
                {
                    if(ValidarTelefono(txtTelefono.Text))
                    {
                        resultado = true;
                    }
                    else
                    {
                        MessageBox.Show("Teléfono inválido. Deben ser todos numeros sin caracteres adicionales.");
                        resultado = false;
                    }
                }
                else
                {
                    MessageBox.Show("DNI inválido. Deben ser todos numeros sin puntos ni caracteres adicionales.");
                    resultado = false;
                }
            }
            else
            {
                resultado = false;
                MessageBox.Show("Por favor, completar todos los campos");
            }

            return resultado;
        }

        void LimpiarCampos()
        {
            txtNombre.Clear();
            txtApellido.Clear();
            txtDNI.Clear();
            txtMail.Clear();
            txtTelefono.Clear();
            lblNombre.Visible = false;
            lblApellido.Visible = false;
            lblDNI.Visible = false;
            lblMail.Visible = false;
            lblTelefono.Visible = false;
            txtNombre.Visible = false;
            txtApellido.Visible = false;
            txtDNI.Visible = false;
            txtMail.Visible = false;
            txtTelefono.Visible = false;
            btnAlta.Visible = false;
            btnCancelar.Visible = false;
            dgvPasajeros.Visible = false;
            rdAlta.Checked = false;
            rdModificar.Checked = false;
            rdBaja.Checked = false;
        }

        public bool ValidarDNI(string valor)
        {
            int i = 0;

            bool resultado = false;

            if (int.TryParse(valor, out i) && valor.Length == 8)
            {
                return true;
            }

            return resultado;
        }

        public bool ValidarTelefono(string valor)
        {
            int i = 0;

            bool resultado = false;

            if (int.TryParse(valor, out i))
            {
                return true;
            }

            return resultado;
        }

        private void btnAlta_Click(object sender, EventArgs e)
        {
            if(rdAlta.Checked)
            {
                if(ValidarCampos())
                {
                    pasajero = new BE.Pasajero();
                    pasajero.Nombre = txtNombre.Text;
                    pasajero.Apellido = txtApellido.Text;
                    pasajero.DNI = int.Parse(txtDNI.Text);
                    pasajero.Mail = txtMail.Text;
                    pasajero.Telefono = int.Parse(txtTelefono.Text);

                    int resultado = gestor_pasajero.InsertarPasajero(pasajero);

                    if (resultado != 1)
                    {
                        MessageBox.Show("Problemas en la escritura");
                    }
                    else
                    {
                        MessageBox.Show("Pasajero registrado exitosamente");
                        Enlazar();
                        LimpiarCampos();
                    }
                }
            }
            else if(rdModificar.Checked)
            {
                if (ValidarCampos())
                {
                    pasajero = (BE.Pasajero)dgvPasajeros.Rows[0].DataBoundItem;
                    pasajero.Nombre = txtNombre.Text;
                    pasajero.Apellido = txtApellido.Text;
                    pasajero.DNI = int.Parse(txtDNI.Text);
                    pasajero.Mail = txtMail.Text;
                    pasajero.Telefono = int.Parse(txtTelefono.Text);

                    int resultado = gestor_pasajero.ModificarPasajero(pasajero);

                    if (resultado != 1)
                    {
                        MessageBox.Show("Problemas en la escritura");
                    }
                    else
                    {
                        MessageBox.Show("Pasajero modificado exitosamente");
                        LimpiarCampos();
                        Enlazar();
                    }
                }
                
            }
            else
            {
                if ((BE.Pasajero)dgvPasajeros.Rows[0].DataBoundItem != null)
                {
                    BE.Pasajero pasajero = (BE.Pasajero)dgvPasajeros.SelectedRows[0].DataBoundItem;

                    int resultado = gestor_pasajero.EliminarPasajero(pasajero);

                    if (resultado != 1)
                    {
                        MessageBox.Show("Problemas en la escritura");
                    }
                    else
                    {
                        MessageBox.Show("Pasajero eliminado exitosamente");
                        LimpiarCampos();
                        Enlazar();
                    }
                }
                else
                {
                    MessageBox.Show("Debe seleccionar un pasajero para eliminar");
                }
                
            }
        }

        private void ABM_Pasajeros_Load(object sender, EventArgs e)
        {
            Enlazar();
        }

        private void dgvPasajeros_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            BE.Pasajero pasajero = (BE.Pasajero)dgvPasajeros.SelectedRows[0].DataBoundItem;
            if (rdModificar.Checked)
            {
                txtNombre.Text = pasajero.Nombre;
                txtApellido.Text = pasajero.Apellido;
                txtDNI.Text = pasajero.DNI.ToString();
                txtMail.Text = pasajero.Mail;
                txtTelefono.Text = pasajero.Telefono.ToString();

            }
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
