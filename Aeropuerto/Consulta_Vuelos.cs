﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Aeropuerto
{
    public partial class Consulta_Vuelos : Form
    {
        BLL.Vuelo gestor_vuelo = new BLL.Vuelo();

        public Consulta_Vuelos()
        {
            InitializeComponent();
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnConsultar_Click(object sender, EventArgs e)
        {
            dgvVuelos.DataSource = null;
            dgvVuelos.DataSource = (from BE.Vuelo v in gestor_vuelo.ListarVuelos()
                                    where int.Parse(v.Ocupacion) < 40 && (int)v.EstadoVuelo == 2
                                    select v).ToList();
        }
    }
}
