﻿namespace Aeropuerto
{
    partial class ABM_Destinos
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dgvDestinos = new System.Windows.Forms.DataGridView();
            this.btnCancelar = new System.Windows.Forms.Button();
            this.btnAlta = new System.Windows.Forms.Button();
            this.txtLongitud = new System.Windows.Forms.TextBox();
            this.txtAeropuerto = new System.Windows.Forms.TextBox();
            this.txtPais = new System.Windows.Forms.TextBox();
            this.txtLatitud = new System.Windows.Forms.TextBox();
            this.txtCiudad = new System.Windows.Forms.TextBox();
            this.rdBaja = new System.Windows.Forms.RadioButton();
            this.rdModificar = new System.Windows.Forms.RadioButton();
            this.rdAlta = new System.Windows.Forms.RadioButton();
            this.lblLongitud = new System.Windows.Forms.Label();
            this.lblAeropuerto = new System.Windows.Forms.Label();
            this.lblPais = new System.Windows.Forms.Label();
            this.lblLatitud = new System.Windows.Forms.Label();
            this.lblCiudad = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dgvDestinos)).BeginInit();
            this.SuspendLayout();
            // 
            // dgvDestinos
            // 
            this.dgvDestinos.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgvDestinos.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvDestinos.Location = new System.Drawing.Point(12, 328);
            this.dgvDestinos.Name = "dgvDestinos";
            this.dgvDestinos.RowHeadersWidth = 51;
            this.dgvDestinos.RowTemplate.Height = 24;
            this.dgvDestinos.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvDestinos.Size = new System.Drawing.Size(828, 285);
            this.dgvDestinos.TabIndex = 33;
            this.dgvDestinos.Visible = false;
            this.dgvDestinos.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvDestinos_CellClick);
            // 
            // btnCancelar
            // 
            this.btnCancelar.Location = new System.Drawing.Point(441, 265);
            this.btnCancelar.Name = "btnCancelar";
            this.btnCancelar.Size = new System.Drawing.Size(140, 45);
            this.btnCancelar.TabIndex = 32;
            this.btnCancelar.Text = "Cancelar";
            this.btnCancelar.UseVisualStyleBackColor = true;
            this.btnCancelar.Visible = false;
            this.btnCancelar.Click += new System.EventHandler(this.btnCancelar_Click);
            // 
            // btnAlta
            // 
            this.btnAlta.Location = new System.Drawing.Point(270, 265);
            this.btnAlta.Name = "btnAlta";
            this.btnAlta.Size = new System.Drawing.Size(140, 45);
            this.btnAlta.TabIndex = 31;
            this.btnAlta.Text = "Registrar Destino";
            this.btnAlta.UseVisualStyleBackColor = true;
            this.btnAlta.Visible = false;
            this.btnAlta.Click += new System.EventHandler(this.btnAlta_Click);
            // 
            // txtLongitud
            // 
            this.txtLongitud.Location = new System.Drawing.Point(279, 168);
            this.txtLongitud.Name = "txtLongitud";
            this.txtLongitud.Size = new System.Drawing.Size(146, 22);
            this.txtLongitud.TabIndex = 30;
            this.txtLongitud.Visible = false;
            // 
            // txtAeropuerto
            // 
            this.txtAeropuerto.Location = new System.Drawing.Point(658, 113);
            this.txtAeropuerto.Name = "txtAeropuerto";
            this.txtAeropuerto.Size = new System.Drawing.Size(146, 22);
            this.txtAeropuerto.TabIndex = 29;
            this.txtAeropuerto.Visible = false;
            // 
            // txtPais
            // 
            this.txtPais.Location = new System.Drawing.Point(381, 109);
            this.txtPais.Name = "txtPais";
            this.txtPais.Size = new System.Drawing.Size(146, 22);
            this.txtPais.TabIndex = 28;
            this.txtPais.Visible = false;
            // 
            // txtLatitud
            // 
            this.txtLatitud.Location = new System.Drawing.Point(537, 168);
            this.txtLatitud.Name = "txtLatitud";
            this.txtLatitud.Size = new System.Drawing.Size(146, 22);
            this.txtLatitud.TabIndex = 27;
            this.txtLatitud.Visible = false;
            // 
            // txtCiudad
            // 
            this.txtCiudad.Location = new System.Drawing.Point(114, 109);
            this.txtCiudad.Name = "txtCiudad";
            this.txtCiudad.Size = new System.Drawing.Size(146, 22);
            this.txtCiudad.TabIndex = 26;
            this.txtCiudad.Visible = false;
            // 
            // rdBaja
            // 
            this.rdBaja.AutoSize = true;
            this.rdBaja.Location = new System.Drawing.Point(583, 56);
            this.rdBaja.Name = "rdBaja";
            this.rdBaja.Size = new System.Drawing.Size(129, 21);
            this.rdBaja.TabIndex = 25;
            this.rdBaja.Text = "Baja de Destino";
            this.rdBaja.UseVisualStyleBackColor = true;
            this.rdBaja.CheckedChanged += new System.EventHandler(this.rdBaja_CheckedChanged);
            // 
            // rdModificar
            // 
            this.rdModificar.AutoSize = true;
            this.rdModificar.Location = new System.Drawing.Point(288, 56);
            this.rdModificar.Name = "rdModificar";
            this.rdModificar.Size = new System.Drawing.Size(138, 21);
            this.rdModificar.TabIndex = 24;
            this.rdModificar.Text = "Modificar Destino";
            this.rdModificar.UseVisualStyleBackColor = true;
            this.rdModificar.CheckedChanged += new System.EventHandler(this.rdModificar_CheckedChanged);
            // 
            // rdAlta
            // 
            this.rdAlta.AutoSize = true;
            this.rdAlta.Location = new System.Drawing.Point(51, 56);
            this.rdAlta.Name = "rdAlta";
            this.rdAlta.Size = new System.Drawing.Size(125, 21);
            this.rdAlta.TabIndex = 23;
            this.rdAlta.Text = "Alta de Destino";
            this.rdAlta.UseVisualStyleBackColor = true;
            this.rdAlta.CheckedChanged += new System.EventHandler(this.rdAlta_CheckedChanged);
            // 
            // lblLongitud
            // 
            this.lblLongitud.AutoSize = true;
            this.lblLongitud.Location = new System.Drawing.Point(195, 168);
            this.lblLongitud.Name = "lblLongitud";
            this.lblLongitud.Size = new System.Drawing.Size(63, 17);
            this.lblLongitud.TabIndex = 22;
            this.lblLongitud.Text = "Longitud";
            this.lblLongitud.Visible = false;
            // 
            // lblAeropuerto
            // 
            this.lblAeropuerto.AutoSize = true;
            this.lblAeropuerto.Location = new System.Drawing.Point(571, 113);
            this.lblAeropuerto.Name = "lblAeropuerto";
            this.lblAeropuerto.Size = new System.Drawing.Size(79, 17);
            this.lblAeropuerto.TabIndex = 21;
            this.lblAeropuerto.Text = "Aeropuerto";
            this.lblAeropuerto.Visible = false;
            // 
            // lblPais
            // 
            this.lblPais.AutoSize = true;
            this.lblPais.Location = new System.Drawing.Point(297, 113);
            this.lblPais.Name = "lblPais";
            this.lblPais.Size = new System.Drawing.Size(35, 17);
            this.lblPais.TabIndex = 20;
            this.lblPais.Text = "Pais";
            this.lblPais.Visible = false;
            // 
            // lblLatitud
            // 
            this.lblLatitud.AutoSize = true;
            this.lblLatitud.Location = new System.Drawing.Point(450, 168);
            this.lblLatitud.Name = "lblLatitud";
            this.lblLatitud.Size = new System.Drawing.Size(51, 17);
            this.lblLatitud.TabIndex = 19;
            this.lblLatitud.Text = "Latitud";
            this.lblLatitud.Visible = false;
            // 
            // lblCiudad
            // 
            this.lblCiudad.AutoSize = true;
            this.lblCiudad.Location = new System.Drawing.Point(27, 109);
            this.lblCiudad.Name = "lblCiudad";
            this.lblCiudad.Size = new System.Drawing.Size(52, 17);
            this.lblCiudad.TabIndex = 18;
            this.lblCiudad.Text = "Ciudad";
            this.lblCiudad.Visible = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(328, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(137, 25);
            this.label1.TabIndex = 17;
            this.label1.Text = "ABM Destinos";
            // 
            // ABM_Destinos
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(851, 628);
            this.Controls.Add(this.dgvDestinos);
            this.Controls.Add(this.btnCancelar);
            this.Controls.Add(this.btnAlta);
            this.Controls.Add(this.txtLongitud);
            this.Controls.Add(this.txtAeropuerto);
            this.Controls.Add(this.txtPais);
            this.Controls.Add(this.txtLatitud);
            this.Controls.Add(this.txtCiudad);
            this.Controls.Add(this.rdBaja);
            this.Controls.Add(this.rdModificar);
            this.Controls.Add(this.rdAlta);
            this.Controls.Add(this.lblLongitud);
            this.Controls.Add(this.lblAeropuerto);
            this.Controls.Add(this.lblPais);
            this.Controls.Add(this.lblLatitud);
            this.Controls.Add(this.lblCiudad);
            this.Controls.Add(this.label1);
            this.Name = "ABM_Destinos";
            this.Text = "Gestion de Destinos";
            this.Load += new System.EventHandler(this.ABM_Destinos_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgvDestinos)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dgvDestinos;
        private System.Windows.Forms.Button btnCancelar;
        private System.Windows.Forms.Button btnAlta;
        private System.Windows.Forms.TextBox txtLongitud;
        private System.Windows.Forms.TextBox txtAeropuerto;
        private System.Windows.Forms.TextBox txtPais;
        private System.Windows.Forms.TextBox txtLatitud;
        private System.Windows.Forms.TextBox txtCiudad;
        private System.Windows.Forms.RadioButton rdBaja;
        private System.Windows.Forms.RadioButton rdModificar;
        private System.Windows.Forms.RadioButton rdAlta;
        private System.Windows.Forms.Label lblLongitud;
        private System.Windows.Forms.Label lblAeropuerto;
        private System.Windows.Forms.Label lblPais;
        private System.Windows.Forms.Label lblLatitud;
        private System.Windows.Forms.Label lblCiudad;
        private System.Windows.Forms.Label label1;
    }
}