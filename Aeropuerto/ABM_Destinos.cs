﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Aeropuerto
{
    public partial class ABM_Destinos : Form
    {
        public ABM_Destinos()
        {
            InitializeComponent();
        }

        BLL.Destino gestor_destino = new BLL.Destino();
        BE.Destino destino = null;

        private void ABM_Destinos_Load(object sender, EventArgs e)
        {
            Enlazar();
        }

        private void rdAlta_CheckedChanged(object sender, EventArgs e)
        {
            lblCiudad.Visible = true;
            lblPais.Visible = true;
            lblAeropuerto.Visible = true;
            lblLatitud.Visible = true;
            lblLongitud.Visible = true;
            txtCiudad.Visible = true;
            txtPais.Visible = true;
            txtAeropuerto.Visible = true;
            txtLatitud.Visible = true;
            txtLongitud.Visible = true;
            btnAlta.Text = "Registrar Destino";
            btnAlta.Visible = true;
            btnCancelar.Visible = true;
            dgvDestinos.Visible = true;
        }

        private void rdModificar_CheckedChanged(object sender, EventArgs e)
        {
            lblCiudad.Visible = true;
            lblPais.Visible = true;
            lblAeropuerto.Visible = true;
            lblLatitud.Visible = true;
            lblLongitud.Visible = true;
            txtCiudad.Visible = true;
            txtPais.Visible = true;
            txtAeropuerto.Visible = true;
            txtLatitud.Visible = true;
            txtLongitud.Visible = true;
            btnAlta.Text = "Modificar Destino";
            btnAlta.Visible = true;
            btnCancelar.Visible = true;
            dgvDestinos.Visible = true;
        }

        private void rdBaja_CheckedChanged(object sender, EventArgs e)
        {
            lblCiudad.Visible = false;
            lblPais.Visible = false;
            lblAeropuerto.Visible = false;
            lblLatitud.Visible = false;
            lblLongitud.Visible = false;
            txtCiudad.Visible = false;
            txtPais.Visible = false;
            txtAeropuerto.Visible = false;
            txtLatitud.Visible = false;
            txtLongitud.Visible = false;
            btnAlta.Text = "Eliminar Destino";
            btnAlta.Visible = true;
            btnCancelar.Visible = true;
            dgvDestinos.Visible = true;
        }
    
        void Enlazar()
        {
            dgvDestinos.DataSource = null;
            dgvDestinos.DataSource = gestor_destino.ListarDestinos();
        }

        bool ValidarCampos()
        {
            bool resultado = false;

            if (!string.IsNullOrWhiteSpace(txtCiudad.Text) && !string.IsNullOrWhiteSpace(txtPais.Text) && !string.IsNullOrWhiteSpace(txtAeropuerto.Text) && !string.IsNullOrWhiteSpace(txtLatitud.Text) && !string.IsNullOrWhiteSpace(txtLongitud.Text))
            {
                resultado = true;
            }
            else
            {
                resultado = false;
                MessageBox.Show("Por favor, completar todos los campos");
            }

            return resultado;
        }

        void LimpiarCampos()
        {
            txtCiudad.Clear();
            txtPais.Clear();
            txtAeropuerto.Clear();
            txtLatitud.Clear();
            txtLongitud.Clear();
            lblCiudad.Visible = false;
            lblPais.Visible = false;
            lblAeropuerto.Visible = false;
            lblLatitud.Visible = false;
            lblLongitud.Visible = false;
            txtCiudad.Visible = false;
            txtPais.Visible = false;
            txtAeropuerto.Visible = false;
            txtLatitud.Visible = false;
            txtLongitud.Visible = false;
            btnAlta.Visible = false;
            btnCancelar.Visible = false;
            dgvDestinos.Visible = false;
        }

        private void btnAlta_Click(object sender, EventArgs e)
        {
            if (rdAlta.Checked)
            {
                if (ValidarCampos())
                {
                    destino = new BE.Destino();
                    destino.Ciudad = txtCiudad.Text;
                    destino.Pais = txtPais.Text;
                    destino.Aeropuerto = txtAeropuerto.Text;
                    destino.Latitud = txtLatitud.Text;
                    destino.Longitud = txtLongitud.Text;

                    int resultado = gestor_destino.InsertarDestino(destino);

                    if (resultado != 1)
                    {
                        MessageBox.Show("Problemas en la escritura");
                    }
                    else
                    {
                        MessageBox.Show("Destino registrado exitosamente");
                        Enlazar();
                        LimpiarCampos();
                    }
                }
            }
            else if (rdModificar.Checked)
            {
                if (ValidarCampos())
                {
                    destino = (BE.Destino)dgvDestinos.SelectedRows[0].DataBoundItem;
                    destino.Ciudad = txtCiudad.Text;
                    destino.Pais = txtPais.Text;
                    destino.Aeropuerto = txtAeropuerto.Text;
                    destino.Latitud = txtLatitud.Text;
                    destino.Longitud = txtLongitud.Text;

                    int resultado = gestor_destino.ModificarDestino(destino);

                    if (resultado != 1)
                    {
                        MessageBox.Show("Problemas en la escritura");
                    }
                    else
                    {
                        MessageBox.Show("Destino modificado exitosamente");
                        LimpiarCampos();
                        Enlazar();
                    }
                }

            }
            else
            {
                if ((BE.Destino)dgvDestinos.Rows[0].DataBoundItem != null)
                {
                    destino = (BE.Destino)dgvDestinos.SelectedRows[0].DataBoundItem;

                    int resultado = gestor_destino.EliminarDestino(destino);

                    if (resultado != 1)
                    {
                        MessageBox.Show("Problemas en la escritura");
                    }
                    else
                    {
                        MessageBox.Show("Destino eliminado exitosamente");
                        LimpiarCampos();
                        Enlazar();
                    }
                }
                else
                {
                    MessageBox.Show("Debe seleccionar un destino para eliminar");
                }

            }
        }

        private void dgvDestinos_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            destino = (BE.Destino)dgvDestinos.SelectedRows[0].DataBoundItem;

            if (rdModificar.Checked)
            {
                txtCiudad.Text = destino.Ciudad;
                txtPais.Text = destino.Pais;
                txtAeropuerto.Text = destino.Aeropuerto;
                txtLatitud.Text = destino.Latitud;
                txtLongitud.Text = destino.Longitud;

            }
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
