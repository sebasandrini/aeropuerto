﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Aeropuerto
{
    public partial class Despegues_Arribos : Form
    {
        public Despegues_Arribos()
        {
            InitializeComponent();
        }

        BLL.Vuelo gestor_vuelos = new BLL.Vuelo();
        BLL.Evento gestor_eventos = new BLL.Evento();
        BE.Vuelo vuelo = null;
        BE.Evento evento = null;

        private void rdDespegue_CheckedChanged(object sender, EventArgs e)
        {
            lblFecha.Visible = true;
            dtFechaEvento.Visible = true;
            btnBuscar.Visible = true;
            btnRegistrarEvento.Text = "Registrar Despegue";

        }

        private void btnBuscar_Click(object sender, EventArgs e)
        {
            lblVuelos.Visible = true;
            dgvVuelos.Visible = true;
            btnCancelar.Visible = true;
            btnRegistrarEvento.Visible = true;
            
            if (ValidarCampos())
            {
                if (rdDespegue.Checked)
                {
                    dgvVuelos.DataSource = null;
                    dgvVuelos.DataSource = (from BE.Vuelo v in gestor_vuelos.ListarVuelos()
                                            where v.EstadoVuelo == (BE.Enum_EstadoVuelo)0
                                            select v).ToList();
                }
                else
                {
                    dgvVuelos.DataSource = null;
                    dgvVuelos.DataSource = (from BE.Vuelo v in gestor_vuelos.ListarVuelos()
                                            where v.EstadoVuelo == (BE.Enum_EstadoVuelo)1
                                            select v).ToList();
                }
            }
        }

        private void rdArribo_CheckedChanged(object sender, EventArgs e)
        {
            lblFecha.Visible = true;
            dtFechaEvento.Visible = true;
            btnBuscar.Visible = true;
            btnRegistrarEvento.Text = "Registrar arribo";
        }

        bool ValidarCampos()
        {
            bool resultado = false;

            if(rdDespegue.Checked || rdArribo.Checked)
            {
                resultado = true;
            }
            else
            {
                MessageBox.Show("Debe seleccionar un despegue o un arribo");
            }

            return resultado;
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnRegistrarEvento_Click(object sender, EventArgs e)
        {
            if (ValidarCampos() && (BE.Vuelo)dgvVuelos.SelectedRows[0].DataBoundItem != null)
            {
                vuelo = (BE.Vuelo)dgvVuelos.SelectedRows[0].DataBoundItem;

                evento = new BE.Evento();

                if (rdDespegue.Checked)
                {
                    vuelo.EstadoVuelo = (BE.Enum_EstadoVuelo)1;
                    evento.Vuelo = vuelo;
                    evento.FechaEvento = dtFechaEvento.Value;
                    evento.TipoEvento = "Despegue";
                    
                    int resultado = gestor_eventos.InsertarEvento(evento);

                    if (resultado > 0)
                    {
                        MessageBox.Show("Despegue registrado con éxito");
                        dgvVuelos.DataSource = null;
                        dgvVuelos.DataSource = (from BE.Vuelo v in gestor_vuelos.ListarVuelos()
                                                where v.EstadoVuelo == (BE.Enum_EstadoVuelo)0
                                                select v).ToList();
                    }
                    else
                    {
                        MessageBox.Show("Error en la escritura");
                    }
                }
                else
                {
                    vuelo.EstadoVuelo = (BE.Enum_EstadoVuelo)2;
                    evento.Vuelo = vuelo;
                    evento.FechaEvento = dtFechaEvento.Value;
                    evento.TipoEvento = "Arribo";

                    int resultado = gestor_eventos.InsertarEvento(evento);

                    if (resultado > 0)
                    {
                        MessageBox.Show("Arribo registrado con éxito");
                        dgvVuelos.DataSource = null;
                        dgvVuelos.DataSource = (from BE.Vuelo v in gestor_vuelos.ListarVuelos()
                                                where v.EstadoVuelo == (BE.Enum_EstadoVuelo)1
                                                select v).ToList();
                    }
                    else
                    {
                        MessageBox.Show("Error en la escritura");
                    }
                }
                
            }
        }

        private void Despegues_Arribos_Load(object sender, EventArgs e)
        {

        }
    }
}
