﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Diagnostics;

namespace Aeropuerto
{
    public partial class Venta_Pasajes : Form
    {
        public Venta_Pasajes()
        {
            InitializeComponent();
        }

        BLL.Pasajero gestor_pasajero = new BLL.Pasajero();
        BLL.Destino gestor_destino = new BLL.Destino();
        BLL.Vuelo gestor_vuelo = new BLL.Vuelo();
        BLL.Pasaje gestor_pasaje = new BLL.Pasaje();
        BE.Vuelo vuelo = null;
        BE.Pasajero pasajero = null;
        BE.Destino destino = null;
        List<BE.Vuelo> vuelos = null;
        List<BE.Pasaje> pasajes = null;
        Detalle_Pasajeros detalle_pasajeros = null;

        private void Venta_Pasajes_Load(object sender, EventArgs e)
        {
            Enlazar();
        }

        void Enlazar()
        {

            cmbPasajeros.DataSource = null;
            cmbPasajeros.DataSource = gestor_pasajero.ListarPasajeros();

            cmbDestinos.DataSource = null;
            cmbDestinos.DataSource = gestor_destino.ListarDestinos();

            dgvVuelos.DataSource = null;
            dgvVuelos.DataSource = gestor_vuelo.ListarVuelos();
        }

        private void btnBuscarVuelos_Click(object sender, EventArgs e)
        {
            vuelos = gestor_vuelo.ListarVuelos();
            pasajes = gestor_pasaje.ListarPasajes();
            vuelos = ObtenerPasajeros(vuelos, pasajes);

            if ((BE.Destino)cmbDestinos.SelectedItem != null)
            {
                if (dtViaje.Value >= DateTime.Now)
                {
                    var q = (from BE.Vuelo v in vuelos
                             where v.FechaPartida >= dtViaje.Value && v.Destino.Id == ((BE.Destino)cmbDestinos.SelectedItem).Id && v.Tipo == cmbTipoVuelo.Text && ((int)v.EstadoVuelo) == 0
                             select v).ToList();

                    if(q.Count>0)
                    {
                        dgvVuelos.DataSource = null;
                        dgvVuelos.DataSource = q;
                        dgvVuelos.Visible = true;
                        lblTotal.Visible = true;
                        txtTotal.Visible = true;
                        btnDetallePasajeros.Visible = true;
                    }
                    else
                    {
                        MessageBox.Show("No hay vuelos para la fecha indicada");
                    }
                    
                }
                else
                {
                    MessageBox.Show("La fecha debe ser a partir del dia de hoy.");
                }
                
            }
            else
            {
                MessageBox.Show("Por favor, seleccione un destino");
            }
            
        }

        private void btnComprarPasaje_Click(object sender, EventArgs e)
        {
            if (ValidarCampos())
            {
                vuelo = (BE.Vuelo)dgvVuelos.SelectedRows[0].DataBoundItem;
                pasajero = (BE.Pasajero)cmbPasajeros.SelectedItem;

                if (BuscarPasajero(pasajero, vuelo))
                {
                    BE.Pasaje pasaje = new BE.Pasaje();
                    pasaje.Pasajero = (BE.Pasajero)cmbPasajeros.SelectedItem;
                    pasaje.Vuelo = (BE.Vuelo)dgvVuelos.SelectedRows[0].DataBoundItem;
                    pasaje.Total = double.Parse(txtTotal.Text);

                    int resultado = gestor_pasaje.InsertarPasaje(pasaje);

                    if (resultado < 0)
                    {
                        MessageBox.Show("Error en la escritura");
                    }
                    else
                    {
                        MessageBox.Show("Pasaje comprado con éxito");
                    }
                }
                else
                {
                    MessageBox.Show("No puede comprar un pasaje dado que usted se encuentra en viaje durante ese periodo");
                }
                
            }
        }

        bool ValidarTotal(string valor)
        {
            int i = 0;

            bool resultado = false;

            if (int.TryParse(valor, out i))
            {
                return true;
            }

            return resultado;
        }

        bool ValidarCampos()
        {
            bool resultado = false;

            if ((BE.Destino)cmbDestinos.SelectedItem != null && (BE.Pasajero)cmbPasajeros.SelectedItem != null && !string.IsNullOrWhiteSpace(txtTotal.Text) && !string.IsNullOrWhiteSpace(cmbTipoVuelo.Text))
            {
                if (ValidarTotal(txtTotal.Text))
                {
                    resultado = true;
                }
                else
                {
                    MessageBox.Show("Por favor, verifique el valor ingresado en el total");
                }
            }
            else
            {
                MessageBox.Show("Por favor, verifique los campos ingresados");
            }

            return resultado;
        }
        
        bool BuscarPasajero (BE.Pasajero pasajero, BE.Vuelo vuelo)
        {
            bool resultado = true;

            foreach(BE.Pasaje p in gestor_pasaje.ListarPasajes())
            {
                if (p.Pasajero.Id == pasajero.Id)
                {
                    if (p.Vuelo.FechaPartida >= vuelo.FechaPartida && p.Vuelo.FechaPartida <= vuelo.FechaLlegada || p.Vuelo.FechaLlegada >= vuelo.FechaPartida && p.Vuelo.FechaLlegada <= vuelo.FechaLlegada)
                    {
                        resultado = false;
                        break;
                    }
                }
            }

            return resultado;
        }

        private void btnDestino_Click(object sender, EventArgs e)
        {
            if ((BE.Destino)cmbDestinos.SelectedItem!=null)
            {
                destino = (BE.Destino)cmbDestinos.SelectedItem;
                Process.Start("chrome", "https://www.google.com.ar/maps/@" + destino.Longitud + "," + destino.Latitud + ",11.78z");
            }
            else
            {
                MessageBox.Show("Debe seleccionar un destino");
            }


        }
    
        List<BE.Vuelo> ObtenerPasajeros (List<BE.Vuelo> vuelos, List<BE.Pasaje> pasajes)
        {
            foreach (BE.Pasaje p in pasajes)
            {
                foreach (BE.Vuelo v in vuelos)
                {
                    if (v.NroVuelo == p.Vuelo.NroVuelo)
                    {
                        v.Pasajeros.Add(p.Pasajero);
                    }
                }
            }
            return vuelos;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if ((BE.Vuelo)dgvVuelos.SelectedRows[0].DataBoundItem != null)
            {
                detalle_pasajeros = new Detalle_Pasajeros();
                detalle_pasajeros.Vuelo = (BE.Vuelo)dgvVuelos.SelectedRows[0].DataBoundItem;
                detalle_pasajeros.ShowDialog();
            }
            else
            {
                MessageBox.Show("Debe seleccionar un vuelo");
            }
            
        }
    }
}
