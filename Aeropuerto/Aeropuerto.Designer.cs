﻿namespace Aeropuerto
{
    partial class Aeropuerto
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.pasajesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.venderPasajeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.despeguesYArribosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.registrarDespegueArriboToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pasajerosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.aBMPasajerosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.vuelosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.aBMVuelosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.consultaDeVuelosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.destinosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.aBMDestinosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.avionesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.aBMAvionesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.pasajesToolStripMenuItem,
            this.despeguesYArribosToolStripMenuItem,
            this.pasajerosToolStripMenuItem,
            this.vuelosToolStripMenuItem,
            this.destinosToolStripMenuItem,
            this.avionesToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(1297, 28);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // pasajesToolStripMenuItem
            // 
            this.pasajesToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.venderPasajeToolStripMenuItem});
            this.pasajesToolStripMenuItem.Name = "pasajesToolStripMenuItem";
            this.pasajesToolStripMenuItem.Size = new System.Drawing.Size(70, 24);
            this.pasajesToolStripMenuItem.Text = "Pasajes";
            // 
            // venderPasajeToolStripMenuItem
            // 
            this.venderPasajeToolStripMenuItem.Name = "venderPasajeToolStripMenuItem";
            this.venderPasajeToolStripMenuItem.Size = new System.Drawing.Size(183, 26);
            this.venderPasajeToolStripMenuItem.Text = "Vender Pasaje";
            this.venderPasajeToolStripMenuItem.Click += new System.EventHandler(this.venderPasajeToolStripMenuItem_Click);
            // 
            // despeguesYArribosToolStripMenuItem
            // 
            this.despeguesYArribosToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.registrarDespegueArriboToolStripMenuItem});
            this.despeguesYArribosToolStripMenuItem.Name = "despeguesYArribosToolStripMenuItem";
            this.despeguesYArribosToolStripMenuItem.Size = new System.Drawing.Size(150, 24);
            this.despeguesYArribosToolStripMenuItem.Text = "Despegues/Arribos";
            // 
            // registrarDespegueArriboToolStripMenuItem
            // 
            this.registrarDespegueArriboToolStripMenuItem.Name = "registrarDespegueArriboToolStripMenuItem";
            this.registrarDespegueArriboToolStripMenuItem.Size = new System.Drawing.Size(270, 26);
            this.registrarDespegueArriboToolStripMenuItem.Text = "Registrar Despegue/Arribo";
            this.registrarDespegueArriboToolStripMenuItem.Click += new System.EventHandler(this.registrarDespegueArriboToolStripMenuItem_Click);
            // 
            // pasajerosToolStripMenuItem
            // 
            this.pasajerosToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.aBMPasajerosToolStripMenuItem});
            this.pasajerosToolStripMenuItem.Name = "pasajerosToolStripMenuItem";
            this.pasajerosToolStripMenuItem.Size = new System.Drawing.Size(84, 24);
            this.pasajerosToolStripMenuItem.Text = "Pasajeros";
            // 
            // aBMPasajerosToolStripMenuItem
            // 
            this.aBMPasajerosToolStripMenuItem.Name = "aBMPasajerosToolStripMenuItem";
            this.aBMPasajerosToolStripMenuItem.Size = new System.Drawing.Size(189, 26);
            this.aBMPasajerosToolStripMenuItem.Text = "ABM Pasajeros";
            this.aBMPasajerosToolStripMenuItem.Click += new System.EventHandler(this.aBMPasajerosToolStripMenuItem_Click);
            // 
            // vuelosToolStripMenuItem
            // 
            this.vuelosToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.aBMVuelosToolStripMenuItem,
            this.consultaDeVuelosToolStripMenuItem});
            this.vuelosToolStripMenuItem.Name = "vuelosToolStripMenuItem";
            this.vuelosToolStripMenuItem.Size = new System.Drawing.Size(67, 24);
            this.vuelosToolStripMenuItem.Text = "Vuelos";
            // 
            // aBMVuelosToolStripMenuItem
            // 
            this.aBMVuelosToolStripMenuItem.Name = "aBMVuelosToolStripMenuItem";
            this.aBMVuelosToolStripMenuItem.Size = new System.Drawing.Size(283, 26);
            this.aBMVuelosToolStripMenuItem.Text = "Alta y Cancelación de Vuelos";
            this.aBMVuelosToolStripMenuItem.Click += new System.EventHandler(this.aBMVuelosToolStripMenuItem_Click);
            // 
            // consultaDeVuelosToolStripMenuItem
            // 
            this.consultaDeVuelosToolStripMenuItem.Name = "consultaDeVuelosToolStripMenuItem";
            this.consultaDeVuelosToolStripMenuItem.Size = new System.Drawing.Size(283, 26);
            this.consultaDeVuelosToolStripMenuItem.Text = "Consulta de Vuelos";
            this.consultaDeVuelosToolStripMenuItem.Click += new System.EventHandler(this.consultaDeVuelosToolStripMenuItem_Click);
            // 
            // destinosToolStripMenuItem
            // 
            this.destinosToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.aBMDestinosToolStripMenuItem});
            this.destinosToolStripMenuItem.Name = "destinosToolStripMenuItem";
            this.destinosToolStripMenuItem.Size = new System.Drawing.Size(80, 24);
            this.destinosToolStripMenuItem.Text = "Destinos";
            // 
            // aBMDestinosToolStripMenuItem
            // 
            this.aBMDestinosToolStripMenuItem.Name = "aBMDestinosToolStripMenuItem";
            this.aBMDestinosToolStripMenuItem.Size = new System.Drawing.Size(185, 26);
            this.aBMDestinosToolStripMenuItem.Text = "ABM Destinos";
            this.aBMDestinosToolStripMenuItem.Click += new System.EventHandler(this.aBMDestinosToolStripMenuItem_Click);
            // 
            // avionesToolStripMenuItem
            // 
            this.avionesToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.aBMAvionesToolStripMenuItem});
            this.avionesToolStripMenuItem.Name = "avionesToolStripMenuItem";
            this.avionesToolStripMenuItem.Size = new System.Drawing.Size(75, 24);
            this.avionesToolStripMenuItem.Text = "Aviones";
            // 
            // aBMAvionesToolStripMenuItem
            // 
            this.aBMAvionesToolStripMenuItem.Name = "aBMAvionesToolStripMenuItem";
            this.aBMAvionesToolStripMenuItem.Size = new System.Drawing.Size(180, 26);
            this.aBMAvionesToolStripMenuItem.Text = "ABM Aviones";
            this.aBMAvionesToolStripMenuItem.Click += new System.EventHandler(this.aBMAvionesToolStripMenuItem_Click);
            // 
            // Aeropuerto
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1297, 903);
            this.Controls.Add(this.menuStrip1);
            this.IsMdiContainer = true;
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Aeropuerto";
            this.Text = "Form1";
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem pasajerosToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem aBMPasajerosToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem vuelosToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem aBMVuelosToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem destinosToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem aBMDestinosToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem avionesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem aBMAvionesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem despeguesYArribosToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem registrarDespegueArriboToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem pasajesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem venderPasajeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem consultaDeVuelosToolStripMenuItem;
    }
}

