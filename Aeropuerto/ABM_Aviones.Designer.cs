﻿namespace Aeropuerto
{
    partial class ABM_Aviones
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dgvAviones = new System.Windows.Forms.DataGridView();
            this.btnCancelar = new System.Windows.Forms.Button();
            this.btnAlta = new System.Windows.Forms.Button();
            this.txtCapacidad = new System.Windows.Forms.TextBox();
            this.rdBaja = new System.Windows.Forms.RadioButton();
            this.rdModificar = new System.Windows.Forms.RadioButton();
            this.rdAlta = new System.Windows.Forms.RadioButton();
            this.lblCapacidad = new System.Windows.Forms.Label();
            this.lblTipo = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.cmbTipo = new System.Windows.Forms.ComboBox();
            ((System.ComponentModel.ISupportInitialize)(this.dgvAviones)).BeginInit();
            this.SuspendLayout();
            // 
            // dgvAviones
            // 
            this.dgvAviones.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvAviones.Location = new System.Drawing.Point(12, 330);
            this.dgvAviones.Name = "dgvAviones";
            this.dgvAviones.RowHeadersWidth = 51;
            this.dgvAviones.RowTemplate.Height = 24;
            this.dgvAviones.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvAviones.Size = new System.Drawing.Size(555, 285);
            this.dgvAviones.TabIndex = 33;
            this.dgvAviones.Visible = false;
            this.dgvAviones.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvAviones_CellClick);
            // 
            // btnCancelar
            // 
            this.btnCancelar.Location = new System.Drawing.Point(285, 268);
            this.btnCancelar.Name = "btnCancelar";
            this.btnCancelar.Size = new System.Drawing.Size(140, 45);
            this.btnCancelar.TabIndex = 32;
            this.btnCancelar.Text = "Cancelar";
            this.btnCancelar.UseVisualStyleBackColor = true;
            this.btnCancelar.Visible = false;
            this.btnCancelar.Click += new System.EventHandler(this.btnCancelar_Click);
            // 
            // btnAlta
            // 
            this.btnAlta.Location = new System.Drawing.Point(114, 268);
            this.btnAlta.Name = "btnAlta";
            this.btnAlta.Size = new System.Drawing.Size(140, 45);
            this.btnAlta.TabIndex = 31;
            this.btnAlta.Text = "Registrar Avion";
            this.btnAlta.UseVisualStyleBackColor = true;
            this.btnAlta.Visible = false;
            this.btnAlta.Click += new System.EventHandler(this.btnAlta_Click);
            // 
            // txtCapacidad
            // 
            this.txtCapacidad.Location = new System.Drawing.Point(371, 174);
            this.txtCapacidad.Name = "txtCapacidad";
            this.txtCapacidad.Size = new System.Drawing.Size(146, 22);
            this.txtCapacidad.TabIndex = 28;
            this.txtCapacidad.Visible = false;
            // 
            // rdBaja
            // 
            this.rdBaja.AutoSize = true;
            this.rdBaja.Location = new System.Drawing.Point(12, 190);
            this.rdBaja.Name = "rdBaja";
            this.rdBaja.Size = new System.Drawing.Size(115, 21);
            this.rdBaja.TabIndex = 25;
            this.rdBaja.Text = "Baja de avion";
            this.rdBaja.UseVisualStyleBackColor = true;
            this.rdBaja.CheckedChanged += new System.EventHandler(this.rdBaja_CheckedChanged);
            // 
            // rdModificar
            // 
            this.rdModificar.AutoSize = true;
            this.rdModificar.Location = new System.Drawing.Point(12, 137);
            this.rdModificar.Name = "rdModificar";
            this.rdModificar.Size = new System.Drawing.Size(125, 21);
            this.rdModificar.TabIndex = 24;
            this.rdModificar.Text = "Modificar Avion";
            this.rdModificar.UseVisualStyleBackColor = true;
            this.rdModificar.CheckedChanged += new System.EventHandler(this.rdModificar_CheckedChanged);
            // 
            // rdAlta
            // 
            this.rdAlta.AutoSize = true;
            this.rdAlta.Location = new System.Drawing.Point(12, 84);
            this.rdAlta.Name = "rdAlta";
            this.rdAlta.Size = new System.Drawing.Size(111, 21);
            this.rdAlta.TabIndex = 23;
            this.rdAlta.Text = "Alta de avion";
            this.rdAlta.UseVisualStyleBackColor = true;
            this.rdAlta.CheckedChanged += new System.EventHandler(this.rdAlta_CheckedChanged);
            // 
            // lblCapacidad
            // 
            this.lblCapacidad.AutoSize = true;
            this.lblCapacidad.Location = new System.Drawing.Point(223, 178);
            this.lblCapacidad.Name = "lblCapacidad";
            this.lblCapacidad.Size = new System.Drawing.Size(75, 17);
            this.lblCapacidad.TabIndex = 20;
            this.lblCapacidad.Text = "Capacidad";
            this.lblCapacidad.Visible = false;
            // 
            // lblTipo
            // 
            this.lblTipo.AutoSize = true;
            this.lblTipo.Location = new System.Drawing.Point(220, 108);
            this.lblTipo.Name = "lblTipo";
            this.lblTipo.Size = new System.Drawing.Size(36, 17);
            this.lblTipo.TabIndex = 18;
            this.lblTipo.Text = "Tipo";
            this.lblTipo.Visible = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(207, 8);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(132, 25);
            this.label1.TabIndex = 17;
            this.label1.Text = "ABM Aviones";
            // 
            // cmbTipo
            // 
            this.cmbTipo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbTipo.FormattingEnabled = true;
            this.cmbTipo.Items.AddRange(new object[] {
            "Boeing 747",
            "Airbus A320",
            "Bombardier CRJ100"});
            this.cmbTipo.Location = new System.Drawing.Point(371, 108);
            this.cmbTipo.Name = "cmbTipo";
            this.cmbTipo.Size = new System.Drawing.Size(146, 24);
            this.cmbTipo.TabIndex = 34;
            this.cmbTipo.Visible = false;
            // 
            // ABM_Aviones
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(586, 618);
            this.Controls.Add(this.cmbTipo);
            this.Controls.Add(this.dgvAviones);
            this.Controls.Add(this.btnCancelar);
            this.Controls.Add(this.btnAlta);
            this.Controls.Add(this.txtCapacidad);
            this.Controls.Add(this.rdBaja);
            this.Controls.Add(this.rdModificar);
            this.Controls.Add(this.rdAlta);
            this.Controls.Add(this.lblCapacidad);
            this.Controls.Add(this.lblTipo);
            this.Controls.Add(this.label1);
            this.Name = "ABM_Aviones";
            this.Text = "Gestion de aviones";
            this.Load += new System.EventHandler(this.ABM_Aviones_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgvAviones)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dgvAviones;
        private System.Windows.Forms.Button btnCancelar;
        private System.Windows.Forms.Button btnAlta;
        private System.Windows.Forms.TextBox txtCapacidad;
        private System.Windows.Forms.RadioButton rdBaja;
        private System.Windows.Forms.RadioButton rdModificar;
        private System.Windows.Forms.RadioButton rdAlta;
        private System.Windows.Forms.Label lblCapacidad;
        private System.Windows.Forms.Label lblTipo;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox cmbTipo;
    }
}