﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Aeropuerto
{
    public partial class Aeropuerto : Form
    {
        public Aeropuerto()
        {
            InitializeComponent();
        }

        ABM_Pasajeros abm_pasajeros = null;
        ABM_Aviones abm_aviones = null;
        ABM_Destinos abm_destinos = null;
        Venta_Pasajes venta_pasajes = null;
        AC_Vuelos ac_vuelos = null;
        Despegues_Arribos despegues_arribos = null;
        Consulta_Vuelos consulta_vuelos = null;

        private void aBMPasajerosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            abm_pasajeros = new ABM_Pasajeros();
            abm_pasajeros.MdiParent = this;
            abm_pasajeros.Show();
        }

        private void aBMAvionesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            abm_aviones = new ABM_Aviones();
            abm_aviones.MdiParent = this;
            abm_aviones.Show();
        }

        private void aBMDestinosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            abm_destinos = new ABM_Destinos();
            abm_destinos.MdiParent = this;
            abm_destinos.Show();
        }

        private void venderPasajeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            venta_pasajes = new Venta_Pasajes();
            venta_pasajes.MdiParent = this;
            venta_pasajes.Show();
        }

        private void aBMVuelosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ac_vuelos = new AC_Vuelos();
            ac_vuelos.MdiParent = this;
            ac_vuelos.Show();
        }

        private void registrarDespegueArriboToolStripMenuItem_Click(object sender, EventArgs e)
        {
            despegues_arribos = new Despegues_Arribos();
            despegues_arribos.MdiParent = this;
            despegues_arribos.Show();
        }

        private void consultaDeVuelosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            consulta_vuelos = new Consulta_Vuelos();
            consulta_vuelos.MdiParent = this;
            consulta_vuelos.Show();
        }
    }
}
