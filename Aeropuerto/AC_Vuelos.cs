﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Aeropuerto
{
    public partial class AC_Vuelos : Form
    {
        public AC_Vuelos()
        {
            InitializeComponent();
        }

        BLL.Avion gestor_avion = new BLL.Avion();
        BLL.Destino gestor_destino = new BLL.Destino();
        BLL.Vuelo gestor_vuelo = new BLL.Vuelo();
        BE.Vuelo vuelo = null;

        private void AC_Vuelos_Load(object sender, EventArgs e)
        {
            Enlazar();
        }
        void Enlazar()
        {
            cmbAvion.DataSource = null;
            cmbAvion.DataSource = gestor_avion.ListarAviones();

            cmbOrigen.DataSource = null;
            cmbOrigen.DataSource = gestor_destino.ListarDestinos(); ;

            cmbDestino.DataSource = null;
            cmbDestino.DataSource = gestor_destino.ListarDestinos(); ;

            dgvVuelos.DataSource = null;
            dgvVuelos.DataSource = gestor_vuelo.ListarVuelos();

            lstDestinos.DataSource = null;
            lstDestinos.DataSource = gestor_destino.ListarDestinos(); ;
        }
        
        bool ValidarCampos()
        {
            bool resultado = false;

            if (dtPartida.Value >= DateTime.Now && dtLlegada.Value >= dtPartida.Value && !string.IsNullOrWhiteSpace(cmbTipoVuelo.Text) && (BE.Destino)cmbOrigen.SelectedItem != null && (BE.Destino)cmbDestino.SelectedItem != null && (BE.Avion)cmbAvion.SelectedItem != null)
            {
                if ((BE.Destino)cmbOrigen.SelectedItem != (BE.Destino)cmbDestino.SelectedItem)
                {
                    resultado = true;
                }
                else
                {
                    MessageBox.Show("El origen y el destino no pueden ser el mismo");
                }
                
            }
            else
            {
                MessageBox.Show("Por favor, validar todos los campos.");
            }

            return resultado;
        }

        private void rdAlta_CheckedChanged(object sender, EventArgs e)
        {
            lblAvion.Visible = true;
            lblDestino.Visible = true;
            lblLlegada.Visible = true;
            lblOrigen.Visible = true;
            lblPartida.Visible = true;
            lblTipoVuelo.Visible = true;
            dtPartida.Visible = true;
            dtLlegada.Visible = true;
            cmbAvion.Visible = true;
            cmbDestino.Visible = true;
            cmbOrigen.Visible = true;
            cmbTipoVuelo.Visible = true;
            btnRegistrarVuelo.Visible = true;
            btnRegistrarVuelo.Text = "Registrar Vuelo";
            btnCancelar.Visible = true;
            lstDestinos.Visible = true;
            lstEscalas.Visible = true;
            btnAgregar.Visible = true;
            btnQuitar.Visible = true;
        }

        private void radioButton1_CheckedChanged(object sender, EventArgs e)
        {
            lblAvion.Visible = false;
            lblDestino.Visible = false;
            lblLlegada.Visible = false;
            lblOrigen.Visible = false;
            lblPartida.Visible = false;
            lblTipoVuelo.Visible = false;
            dtPartida.Visible = false;
            dtLlegada.Visible = false;
            cmbAvion.Visible = false;
            cmbDestino.Visible = false;
            cmbOrigen.Visible = false;
            cmbTipoVuelo.Visible = false;
            btnRegistrarVuelo.Visible = true;
            btnRegistrarVuelo.Text = "Cancelar Vuelo";
            btnCancelar.Visible = true;
            lstDestinos.Visible = false;
            lstEscalas.Visible = false;
            btnAgregar.Visible = false;
            btnQuitar.Visible = false;
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnRegistrarVuelo_Click(object sender, EventArgs e)
        {
            if (rdAlta.Checked)
            {
                if(ValidarCampos() && ValidarEscalas())
                {
                    vuelo = new BE.Vuelo();
                    vuelo.FechaPartida = dtPartida.Value;
                    vuelo.FechaLlegada = dtLlegada.Value;
                    vuelo.Avion = (BE.Avion)cmbAvion.SelectedItem;
                    vuelo.Tipo = cmbTipoVuelo.Text;
                    vuelo.Origen = (BE.Destino)cmbOrigen.SelectedItem;
                    vuelo.Destino = (BE.Destino)cmbDestino.SelectedItem;

                    foreach (BE.Destino d in lstEscalas.Items)
                    {
                        vuelo.Escalas.Add(d);
                    }
                    
                    int resultado = gestor_vuelo.InsertarVuelo(vuelo);

                    if (resultado != 1)
                    {
                        MessageBox.Show("Error en la escritura");
                    }
                    else
                    {
                        MessageBox.Show("Vuelo registrado con éxito");
                        Enlazar();
                    }
                }
            }
            else
            {
                if ((BE.Vuelo)dgvVuelos.SelectedRows[0].DataBoundItem != null)
                {
                    vuelo = (BE.Vuelo)dgvVuelos.SelectedRows[0].DataBoundItem;
                    int resultado = gestor_vuelo.CancelarVuelo(vuelo);

                    if (resultado != 1)
                    {
                        MessageBox.Show("Error en la escritura");
                    }
                    else
                    {
                        MessageBox.Show("Vuelo registrado con éxito");
                        Enlazar();
                    }
                }
                else
                {
                    MessageBox.Show("Debe seleccionar un vuelo para cancelar");
                }
            }
        }
        
        bool ValidarEscalas ()
        {
            bool resultado = true;

            foreach (BE.Destino d in lstEscalas.Items)
            {
                if (d.Id == ((BE.Destino)cmbOrigen.SelectedItem).Id || d.Id == ((BE.Destino)cmbDestino.SelectedItem).Id)
                {
                    resultado = false;
                }
            }
            if (!resultado)
            {
                MessageBox.Show("Verifique la lista de escalas dado que no pueden contener ni al origen ni al destino");
            }

            return resultado;
        }

        private void btnAgregar_Click(object sender, EventArgs e)
        {
            if((BE.Destino)lstDestinos.SelectedItem != null)
            {
                lstEscalas.Items.Add((BE.Destino)lstDestinos.SelectedItem);
            }
            else
            {
                MessageBox.Show("Por favor, seleccione un destino");
            }
        }

        private void btnQuitar_Click(object sender, EventArgs e)
        {
            if ((BE.Destino)lstEscalas.SelectedItem != null)
            {
                lstEscalas.Items.Remove((BE.Destino)lstEscalas.SelectedItem);
            }
            else
            {
                MessageBox.Show("Por favor, seleccione una escala");
            }
        }
    }
}
