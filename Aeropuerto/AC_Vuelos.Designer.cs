﻿namespace Aeropuerto
{
    partial class AC_Vuelos
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.dgvVuelos = new System.Windows.Forms.DataGridView();
            this.lblPartida = new System.Windows.Forms.Label();
            this.lblLlegada = new System.Windows.Forms.Label();
            this.lblOrigen = new System.Windows.Forms.Label();
            this.lblDestino = new System.Windows.Forms.Label();
            this.lblAvion = new System.Windows.Forms.Label();
            this.lblTipoVuelo = new System.Windows.Forms.Label();
            this.dtPartida = new System.Windows.Forms.DateTimePicker();
            this.dtLlegada = new System.Windows.Forms.DateTimePicker();
            this.cmbTipoVuelo = new System.Windows.Forms.ComboBox();
            this.cmbOrigen = new System.Windows.Forms.ComboBox();
            this.cmbDestino = new System.Windows.Forms.ComboBox();
            this.cmbAvion = new System.Windows.Forms.ComboBox();
            this.btnRegistrarVuelo = new System.Windows.Forms.Button();
            this.btnCancelar = new System.Windows.Forms.Button();
            this.rdAlta = new System.Windows.Forms.RadioButton();
            this.rdCancelar = new System.Windows.Forms.RadioButton();
            this.lstDestinos = new System.Windows.Forms.ListBox();
            this.lstEscalas = new System.Windows.Forms.ListBox();
            this.btnAgregar = new System.Windows.Forms.Button();
            this.btnQuitar = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dgvVuelos)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(425, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(296, 25);
            this.label1.TabIndex = 0;
            this.label1.Text = "Alta y Cancelacion de Vuelos";
            // 
            // dgvVuelos
            // 
            this.dgvVuelos.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgvVuelos.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvVuelos.Location = new System.Drawing.Point(12, 406);
            this.dgvVuelos.Name = "dgvVuelos";
            this.dgvVuelos.RowHeadersWidth = 51;
            this.dgvVuelos.RowTemplate.Height = 24;
            this.dgvVuelos.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvVuelos.Size = new System.Drawing.Size(1128, 330);
            this.dgvVuelos.TabIndex = 1;
            // 
            // lblPartida
            // 
            this.lblPartida.AutoSize = true;
            this.lblPartida.Location = new System.Drawing.Point(35, 120);
            this.lblPartida.Name = "lblPartida";
            this.lblPartida.Size = new System.Drawing.Size(115, 17);
            this.lblPartida.TabIndex = 2;
            this.lblPartida.Text = "Fecha de partida";
            this.lblPartida.Visible = false;
            // 
            // lblLlegada
            // 
            this.lblLlegada.AutoSize = true;
            this.lblLlegada.Location = new System.Drawing.Point(33, 159);
            this.lblLlegada.Name = "lblLlegada";
            this.lblLlegada.Size = new System.Drawing.Size(117, 17);
            this.lblLlegada.TabIndex = 3;
            this.lblLlegada.Text = "Fecha de llegada";
            this.lblLlegada.Visible = false;
            // 
            // lblOrigen
            // 
            this.lblOrigen.AutoSize = true;
            this.lblOrigen.Location = new System.Drawing.Point(35, 227);
            this.lblOrigen.Name = "lblOrigen";
            this.lblOrigen.Size = new System.Drawing.Size(51, 17);
            this.lblOrigen.TabIndex = 4;
            this.lblOrigen.Text = "Origen";
            this.lblOrigen.Visible = false;
            // 
            // lblDestino
            // 
            this.lblDestino.AutoSize = true;
            this.lblDestino.Location = new System.Drawing.Point(33, 262);
            this.lblDestino.Name = "lblDestino";
            this.lblDestino.Size = new System.Drawing.Size(56, 17);
            this.lblDestino.TabIndex = 5;
            this.lblDestino.Text = "Destino";
            this.lblDestino.Visible = false;
            // 
            // lblAvion
            // 
            this.lblAvion.AutoSize = true;
            this.lblAvion.Location = new System.Drawing.Point(35, 299);
            this.lblAvion.Name = "lblAvion";
            this.lblAvion.Size = new System.Drawing.Size(43, 17);
            this.lblAvion.TabIndex = 6;
            this.lblAvion.Text = "Avion";
            this.lblAvion.Visible = false;
            // 
            // lblTipoVuelo
            // 
            this.lblTipoVuelo.AutoSize = true;
            this.lblTipoVuelo.Location = new System.Drawing.Point(33, 195);
            this.lblTipoVuelo.Name = "lblTipoVuelo";
            this.lblTipoVuelo.Size = new System.Drawing.Size(94, 17);
            this.lblTipoVuelo.TabIndex = 7;
            this.lblTipoVuelo.Text = "Tipo de vuelo";
            this.lblTipoVuelo.Visible = false;
            // 
            // dtPartida
            // 
            this.dtPartida.CustomFormat = "dd-MM/yyyy HH:mm";
            this.dtPartida.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtPartida.Location = new System.Drawing.Point(205, 115);
            this.dtPartida.Name = "dtPartida";
            this.dtPartida.Size = new System.Drawing.Size(200, 22);
            this.dtPartida.TabIndex = 8;
            this.dtPartida.Visible = false;
            // 
            // dtLlegada
            // 
            this.dtLlegada.CustomFormat = "dd/MM/yyyy HH:mm";
            this.dtLlegada.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtLlegada.Location = new System.Drawing.Point(205, 154);
            this.dtLlegada.Name = "dtLlegada";
            this.dtLlegada.Size = new System.Drawing.Size(200, 22);
            this.dtLlegada.TabIndex = 9;
            this.dtLlegada.Visible = false;
            // 
            // cmbTipoVuelo
            // 
            this.cmbTipoVuelo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbTipoVuelo.FormattingEnabled = true;
            this.cmbTipoVuelo.Items.AddRange(new object[] {
            "Nacional",
            "Internacional"});
            this.cmbTipoVuelo.Location = new System.Drawing.Point(205, 188);
            this.cmbTipoVuelo.Name = "cmbTipoVuelo";
            this.cmbTipoVuelo.Size = new System.Drawing.Size(200, 24);
            this.cmbTipoVuelo.TabIndex = 10;
            this.cmbTipoVuelo.Visible = false;
            // 
            // cmbOrigen
            // 
            this.cmbOrigen.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbOrigen.FormattingEnabled = true;
            this.cmbOrigen.Location = new System.Drawing.Point(205, 220);
            this.cmbOrigen.Name = "cmbOrigen";
            this.cmbOrigen.Size = new System.Drawing.Size(200, 24);
            this.cmbOrigen.TabIndex = 11;
            this.cmbOrigen.Visible = false;
            // 
            // cmbDestino
            // 
            this.cmbDestino.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbDestino.FormattingEnabled = true;
            this.cmbDestino.Location = new System.Drawing.Point(205, 255);
            this.cmbDestino.Name = "cmbDestino";
            this.cmbDestino.Size = new System.Drawing.Size(200, 24);
            this.cmbDestino.TabIndex = 12;
            this.cmbDestino.Visible = false;
            // 
            // cmbAvion
            // 
            this.cmbAvion.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbAvion.FormattingEnabled = true;
            this.cmbAvion.Location = new System.Drawing.Point(205, 292);
            this.cmbAvion.Name = "cmbAvion";
            this.cmbAvion.Size = new System.Drawing.Size(200, 24);
            this.cmbAvion.TabIndex = 13;
            this.cmbAvion.Visible = false;
            // 
            // btnRegistrarVuelo
            // 
            this.btnRegistrarVuelo.Location = new System.Drawing.Point(38, 342);
            this.btnRegistrarVuelo.Name = "btnRegistrarVuelo";
            this.btnRegistrarVuelo.Size = new System.Drawing.Size(125, 41);
            this.btnRegistrarVuelo.TabIndex = 14;
            this.btnRegistrarVuelo.Text = "Registrar Vuelo";
            this.btnRegistrarVuelo.UseVisualStyleBackColor = true;
            this.btnRegistrarVuelo.Visible = false;
            this.btnRegistrarVuelo.Click += new System.EventHandler(this.btnRegistrarVuelo_Click);
            // 
            // btnCancelar
            // 
            this.btnCancelar.Location = new System.Drawing.Point(219, 342);
            this.btnCancelar.Name = "btnCancelar";
            this.btnCancelar.Size = new System.Drawing.Size(125, 41);
            this.btnCancelar.TabIndex = 15;
            this.btnCancelar.Text = "Cancelar";
            this.btnCancelar.UseVisualStyleBackColor = true;
            this.btnCancelar.Visible = false;
            this.btnCancelar.Click += new System.EventHandler(this.btnCancelar_Click);
            // 
            // rdAlta
            // 
            this.rdAlta.AutoSize = true;
            this.rdAlta.Location = new System.Drawing.Point(74, 66);
            this.rdAlta.Name = "rdAlta";
            this.rdAlta.Size = new System.Drawing.Size(113, 21);
            this.rdAlta.TabIndex = 16;
            this.rdAlta.TabStop = true;
            this.rdAlta.Text = "Alta de Vuelo";
            this.rdAlta.UseVisualStyleBackColor = true;
            this.rdAlta.CheckedChanged += new System.EventHandler(this.rdAlta_CheckedChanged);
            // 
            // rdCancelar
            // 
            this.rdCancelar.AutoSize = true;
            this.rdCancelar.Location = new System.Drawing.Point(277, 66);
            this.rdCancelar.Name = "rdCancelar";
            this.rdCancelar.Size = new System.Drawing.Size(166, 21);
            this.rdCancelar.TabIndex = 17;
            this.rdCancelar.TabStop = true;
            this.rdCancelar.Text = "Cancelación de Vuelo";
            this.rdCancelar.UseVisualStyleBackColor = true;
            this.rdCancelar.CheckedChanged += new System.EventHandler(this.radioButton1_CheckedChanged);
            // 
            // lstDestinos
            // 
            this.lstDestinos.FormattingEnabled = true;
            this.lstDestinos.ItemHeight = 16;
            this.lstDestinos.Location = new System.Drawing.Point(562, 99);
            this.lstDestinos.Name = "lstDestinos";
            this.lstDestinos.Size = new System.Drawing.Size(159, 260);
            this.lstDestinos.TabIndex = 18;
            this.lstDestinos.Visible = false;
            // 
            // lstEscalas
            // 
            this.lstEscalas.FormattingEnabled = true;
            this.lstEscalas.ItemHeight = 16;
            this.lstEscalas.Location = new System.Drawing.Point(922, 99);
            this.lstEscalas.Name = "lstEscalas";
            this.lstEscalas.Size = new System.Drawing.Size(159, 260);
            this.lstEscalas.TabIndex = 19;
            this.lstEscalas.Visible = false;
            // 
            // btnAgregar
            // 
            this.btnAgregar.Location = new System.Drawing.Point(775, 128);
            this.btnAgregar.Name = "btnAgregar";
            this.btnAgregar.Size = new System.Drawing.Size(100, 70);
            this.btnAgregar.TabIndex = 20;
            this.btnAgregar.Text = "Agregar Escala\r\n--->";
            this.btnAgregar.UseVisualStyleBackColor = true;
            this.btnAgregar.Visible = false;
            this.btnAgregar.Click += new System.EventHandler(this.btnAgregar_Click);
            // 
            // btnQuitar
            // 
            this.btnQuitar.Location = new System.Drawing.Point(775, 227);
            this.btnQuitar.Name = "btnQuitar";
            this.btnQuitar.Size = new System.Drawing.Size(100, 75);
            this.btnQuitar.TabIndex = 21;
            this.btnQuitar.Text = "<---\r\nQuitar Escala";
            this.btnQuitar.UseVisualStyleBackColor = true;
            this.btnQuitar.Visible = false;
            this.btnQuitar.Click += new System.EventHandler(this.btnQuitar_Click);
            // 
            // AC_Vuelos
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1152, 757);
            this.Controls.Add(this.btnQuitar);
            this.Controls.Add(this.btnAgregar);
            this.Controls.Add(this.lstEscalas);
            this.Controls.Add(this.lstDestinos);
            this.Controls.Add(this.rdCancelar);
            this.Controls.Add(this.rdAlta);
            this.Controls.Add(this.btnCancelar);
            this.Controls.Add(this.btnRegistrarVuelo);
            this.Controls.Add(this.cmbAvion);
            this.Controls.Add(this.cmbDestino);
            this.Controls.Add(this.cmbOrigen);
            this.Controls.Add(this.cmbTipoVuelo);
            this.Controls.Add(this.dtLlegada);
            this.Controls.Add(this.dtPartida);
            this.Controls.Add(this.lblTipoVuelo);
            this.Controls.Add(this.lblAvion);
            this.Controls.Add(this.lblDestino);
            this.Controls.Add(this.lblOrigen);
            this.Controls.Add(this.lblLlegada);
            this.Controls.Add(this.lblPartida);
            this.Controls.Add(this.dgvVuelos);
            this.Controls.Add(this.label1);
            this.Name = "AC_Vuelos";
            this.Text = "Alta y Cancelacion de Vuelos";
            this.Load += new System.EventHandler(this.AC_Vuelos_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgvVuelos)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DataGridView dgvVuelos;
        private System.Windows.Forms.Label lblPartida;
        private System.Windows.Forms.Label lblLlegada;
        private System.Windows.Forms.Label lblOrigen;
        private System.Windows.Forms.Label lblDestino;
        private System.Windows.Forms.Label lblAvion;
        private System.Windows.Forms.Label lblTipoVuelo;
        private System.Windows.Forms.DateTimePicker dtPartida;
        private System.Windows.Forms.DateTimePicker dtLlegada;
        private System.Windows.Forms.ComboBox cmbTipoVuelo;
        private System.Windows.Forms.ComboBox cmbOrigen;
        private System.Windows.Forms.ComboBox cmbDestino;
        private System.Windows.Forms.ComboBox cmbAvion;
        private System.Windows.Forms.Button btnRegistrarVuelo;
        private System.Windows.Forms.Button btnCancelar;
        private System.Windows.Forms.RadioButton rdAlta;
        private System.Windows.Forms.RadioButton rdCancelar;
        private System.Windows.Forms.ListBox lstDestinos;
        private System.Windows.Forms.ListBox lstEscalas;
        private System.Windows.Forms.Button btnAgregar;
        private System.Windows.Forms.Button btnQuitar;
    }
}