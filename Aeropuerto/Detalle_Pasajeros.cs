﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Aeropuerto
{
    public partial class Detalle_Pasajeros : Form
    {
        public Detalle_Pasajeros()
        {
            InitializeComponent();
        }

        private BE.Vuelo vuelo;

        public BE.Vuelo Vuelo
        {
            get { return vuelo; }
            set { vuelo = value; }
        }

        private void Detalle_Pasajeros_Load(object sender, EventArgs e)
        {
            dgvPasajeros.DataSource = null;
            dgvPasajeros.DataSource = this.vuelo.Pasajeros;
            lblNroVuelo.Text = vuelo.NroVuelo.ToString();

        }

        private void btnSalir_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
