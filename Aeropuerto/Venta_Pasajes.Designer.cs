﻿namespace Aeropuerto
{
    partial class Venta_Pasajes
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.lblPasajero = new System.Windows.Forms.Label();
            this.cmbPasajeros = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.cmbDestinos = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.dtViaje = new System.Windows.Forms.DateTimePicker();
            this.btnBuscarVuelos = new System.Windows.Forms.Button();
            this.dgvVuelos = new System.Windows.Forms.DataGridView();
            this.btnComprarPasaje = new System.Windows.Forms.Button();
            this.lblTotal = new System.Windows.Forms.Label();
            this.txtTotal = new System.Windows.Forms.TextBox();
            this.lblTipoVuelo = new System.Windows.Forms.Label();
            this.cmbTipoVuelo = new System.Windows.Forms.ComboBox();
            this.btnDestino = new System.Windows.Forms.Button();
            this.btnDetallePasajeros = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dgvVuelos)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(261, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(180, 25);
            this.label1.TabIndex = 0;
            this.label1.Text = "Venta de pasajes";
            // 
            // lblPasajero
            // 
            this.lblPasajero.AutoSize = true;
            this.lblPasajero.Location = new System.Drawing.Point(15, 437);
            this.lblPasajero.Name = "lblPasajero";
            this.lblPasajero.Size = new System.Drawing.Size(64, 17);
            this.lblPasajero.TabIndex = 1;
            this.lblPasajero.Text = "Pasajero";
            // 
            // cmbPasajeros
            // 
            this.cmbPasajeros.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbPasajeros.FormattingEnabled = true;
            this.cmbPasajeros.Location = new System.Drawing.Point(105, 437);
            this.cmbPasajeros.Name = "cmbPasajeros";
            this.cmbPasajeros.Size = new System.Drawing.Size(156, 24);
            this.cmbPasajeros.TabIndex = 2;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 68);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(56, 17);
            this.label3.TabIndex = 3;
            this.label3.Text = "Destino";
            // 
            // cmbDestinos
            // 
            this.cmbDestinos.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbDestinos.FormattingEnabled = true;
            this.cmbDestinos.Location = new System.Drawing.Point(84, 68);
            this.cmbDestinos.Name = "cmbDestinos";
            this.cmbDestinos.Size = new System.Drawing.Size(156, 24);
            this.cmbDestinos.TabIndex = 4;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(263, 71);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(100, 17);
            this.label4.TabIndex = 5;
            this.label4.Text = "Fecha de viaje";
            // 
            // dtViaje
            // 
            this.dtViaje.CustomFormat = "dd/MM/yyyy HH:mm";
            this.dtViaje.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtViaje.Location = new System.Drawing.Point(374, 70);
            this.dtViaje.Name = "dtViaje";
            this.dtViaje.Size = new System.Drawing.Size(200, 22);
            this.dtViaje.TabIndex = 6;
            // 
            // btnBuscarVuelos
            // 
            this.btnBuscarVuelos.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnBuscarVuelos.Location = new System.Drawing.Point(564, 132);
            this.btnBuscarVuelos.Name = "btnBuscarVuelos";
            this.btnBuscarVuelos.Size = new System.Drawing.Size(272, 36);
            this.btnBuscarVuelos.TabIndex = 7;
            this.btnBuscarVuelos.Text = "Buscar Vuelos";
            this.btnBuscarVuelos.UseVisualStyleBackColor = true;
            this.btnBuscarVuelos.Click += new System.EventHandler(this.btnBuscarVuelos_Click);
            // 
            // dgvVuelos
            // 
            this.dgvVuelos.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvVuelos.Location = new System.Drawing.Point(15, 197);
            this.dgvVuelos.Name = "dgvVuelos";
            this.dgvVuelos.RowHeadersWidth = 51;
            this.dgvVuelos.RowTemplate.Height = 24;
            this.dgvVuelos.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvVuelos.Size = new System.Drawing.Size(813, 207);
            this.dgvVuelos.TabIndex = 8;
            this.dgvVuelos.Visible = false;
            // 
            // btnComprarPasaje
            // 
            this.btnComprarPasaje.Location = new System.Drawing.Point(619, 429);
            this.btnComprarPasaje.Name = "btnComprarPasaje";
            this.btnComprarPasaje.Size = new System.Drawing.Size(146, 39);
            this.btnComprarPasaje.TabIndex = 9;
            this.btnComprarPasaje.Text = "Comprar Pasaje";
            this.btnComprarPasaje.UseVisualStyleBackColor = true;
            this.btnComprarPasaje.Click += new System.EventHandler(this.btnComprarPasaje_Click);
            // 
            // lblTotal
            // 
            this.lblTotal.AutoSize = true;
            this.lblTotal.Location = new System.Drawing.Point(309, 443);
            this.lblTotal.Name = "lblTotal";
            this.lblTotal.Size = new System.Drawing.Size(40, 17);
            this.lblTotal.TabIndex = 10;
            this.lblTotal.Text = "Total";
            // 
            // txtTotal
            // 
            this.txtTotal.Location = new System.Drawing.Point(374, 438);
            this.txtTotal.Name = "txtTotal";
            this.txtTotal.Size = new System.Drawing.Size(165, 22);
            this.txtTotal.TabIndex = 11;
            this.txtTotal.Visible = false;
            // 
            // lblTipoVuelo
            // 
            this.lblTipoVuelo.AutoSize = true;
            this.lblTipoVuelo.Location = new System.Drawing.Point(595, 71);
            this.lblTipoVuelo.Name = "lblTipoVuelo";
            this.lblTipoVuelo.Size = new System.Drawing.Size(94, 17);
            this.lblTipoVuelo.TabIndex = 12;
            this.lblTipoVuelo.Text = "Tipo de vuelo";
            // 
            // cmbTipoVuelo
            // 
            this.cmbTipoVuelo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbTipoVuelo.FormattingEnabled = true;
            this.cmbTipoVuelo.Items.AddRange(new object[] {
            "Nacional",
            "Internacional"});
            this.cmbTipoVuelo.Location = new System.Drawing.Point(695, 68);
            this.cmbTipoVuelo.Name = "cmbTipoVuelo";
            this.cmbTipoVuelo.Size = new System.Drawing.Size(133, 24);
            this.cmbTipoVuelo.TabIndex = 13;
            // 
            // btnDestino
            // 
            this.btnDestino.Location = new System.Drawing.Point(28, 132);
            this.btnDestino.Name = "btnDestino";
            this.btnDestino.Size = new System.Drawing.Size(252, 36);
            this.btnDestino.TabIndex = 14;
            this.btnDestino.Text = "Visualizar destino en Chrome";
            this.btnDestino.UseVisualStyleBackColor = true;
            this.btnDestino.Click += new System.EventHandler(this.btnDestino_Click);
            // 
            // btnDetallePasajeros
            // 
            this.btnDetallePasajeros.Location = new System.Drawing.Point(286, 132);
            this.btnDetallePasajeros.Name = "btnDetallePasajeros";
            this.btnDetallePasajeros.Size = new System.Drawing.Size(272, 36);
            this.btnDetallePasajeros.TabIndex = 15;
            this.btnDetallePasajeros.Text = "Ver detalle de pasajeros";
            this.btnDetallePasajeros.UseVisualStyleBackColor = true;
            this.btnDetallePasajeros.Visible = false;
            this.btnDetallePasajeros.Click += new System.EventHandler(this.button1_Click);
            // 
            // Venta_Pasajes
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(840, 483);
            this.Controls.Add(this.btnDetallePasajeros);
            this.Controls.Add(this.btnDestino);
            this.Controls.Add(this.cmbTipoVuelo);
            this.Controls.Add(this.lblTipoVuelo);
            this.Controls.Add(this.txtTotal);
            this.Controls.Add(this.lblTotal);
            this.Controls.Add(this.btnComprarPasaje);
            this.Controls.Add(this.dgvVuelos);
            this.Controls.Add(this.btnBuscarVuelos);
            this.Controls.Add(this.dtViaje);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.cmbDestinos);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.cmbPasajeros);
            this.Controls.Add(this.lblPasajero);
            this.Controls.Add(this.label1);
            this.Name = "Venta_Pasajes";
            this.Text = "Venta de Pasajes";
            this.Load += new System.EventHandler(this.Venta_Pasajes_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgvVuelos)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lblPasajero;
        private System.Windows.Forms.ComboBox cmbPasajeros;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox cmbDestinos;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.DateTimePicker dtViaje;
        private System.Windows.Forms.Button btnBuscarVuelos;
        private System.Windows.Forms.DataGridView dgvVuelos;
        private System.Windows.Forms.Button btnComprarPasaje;
        private System.Windows.Forms.Label lblTotal;
        private System.Windows.Forms.TextBox txtTotal;
        private System.Windows.Forms.Label lblTipoVuelo;
        private System.Windows.Forms.ComboBox cmbTipoVuelo;
        private System.Windows.Forms.Button btnDestino;
        private System.Windows.Forms.Button btnDetallePasajeros;
    }
}