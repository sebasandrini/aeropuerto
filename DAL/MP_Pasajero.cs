﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;

namespace DAL
{
    public class MP_Pasajero
    {
        Acceso acceso = new Acceso();
        
        public int InsertarPasajero(BE.Pasajero pasajero)
        {
            List<SqlParameter> parametros = new List<SqlParameter>();

            SqlParameter nombre = new SqlParameter();
            nombre.ParameterName = "@nombre";
            nombre.Value = pasajero.Nombre;
            nombre.SqlDbType = SqlDbType.Text;
            parametros.Add(nombre);

            SqlParameter apellido = new SqlParameter();
            apellido.ParameterName = "@apellido";
            apellido.Value = pasajero.Apellido;
            apellido.SqlDbType = SqlDbType.Text;
            parametros.Add(apellido);

            SqlParameter mail = new SqlParameter();
            mail.ParameterName = "@mail";
            mail.Value = pasajero.Mail;
            mail.SqlDbType = SqlDbType.Text;
            parametros.Add(mail);

            SqlParameter dni = new SqlParameter();
            dni.ParameterName = "@dni";
            dni.Value = pasajero.DNI;
            dni.SqlDbType = SqlDbType.Int;
            parametros.Add(dni);

            SqlParameter telefono = new SqlParameter();
            telefono.ParameterName = "@telefono";
            telefono.Value = pasajero.Telefono;
            telefono.SqlDbType = SqlDbType.Int;
            parametros.Add(telefono);

            string SQL = "InsertarPasajero";

            int resultado = acceso.Escribir(SQL, parametros);

            return resultado;
        }

        public int ModificarPasajero(BE.Pasajero pasajero)
        {
            List<SqlParameter> parametros = new List<SqlParameter>();

            SqlParameter id = new SqlParameter();
            id.ParameterName = "@id";
            id.Value = pasajero.Id;
            id.SqlDbType = SqlDbType.Int;
            parametros.Add(id);

            SqlParameter nombre = new SqlParameter();
            nombre.ParameterName = "@nombre";
            nombre.Value = pasajero.Nombre;
            nombre.SqlDbType = SqlDbType.Text;
            parametros.Add(nombre);

            SqlParameter apellido = new SqlParameter();
            apellido.ParameterName = "@apellido";
            apellido.Value = pasajero.Apellido;
            apellido.SqlDbType = SqlDbType.Text;
            parametros.Add(apellido);

            SqlParameter mail = new SqlParameter();
            mail.ParameterName = "@mail";
            mail.Value = pasajero.Mail;
            mail.SqlDbType = SqlDbType.Text;
            parametros.Add(mail);

            SqlParameter dni = new SqlParameter();
            dni.ParameterName = "@dni";
            dni.Value = pasajero.DNI;
            dni.SqlDbType = SqlDbType.Int;
            parametros.Add(dni);

            SqlParameter telefono = new SqlParameter();
            telefono.ParameterName = "@telefono";
            telefono.Value = pasajero.Telefono;
            telefono.SqlDbType = SqlDbType.Int;
            parametros.Add(telefono);

            string SQL = "ModificarPasajero";

            int resultado = acceso.Escribir(SQL, parametros);

            return resultado;
        }
        
        public int EliminarPasajero(BE.Pasajero pasajero)
        {
            List<SqlParameter> parametros = new List<SqlParameter>();

            SqlParameter id = new SqlParameter();
            id.ParameterName = "@id";
            id.Value = pasajero.Id;
            id.SqlDbType = SqlDbType.Int;
            parametros.Add(id);

            string SQL = "EliminarPasajero";

            int resultado = acceso.Escribir(SQL, parametros);

            return resultado;
        }

        public List<BE.Pasajero> ListarPasajeros()
        {
            List<BE.Pasajero> pasajeros = new List<BE.Pasajero>();

            acceso.Abrir();

            DataTable tabla = acceso.Leer("ListarPasajeros");

            foreach (DataRow registro in tabla.Rows)
            {
                if(int.Parse(registro["activo"].ToString()) != 0)
                {
                    BE.Pasajero pasajero = new BE.Pasajero();

                    pasajero.Id = int.Parse(registro["id"].ToString());
                    pasajero.Nombre = registro["nombre"].ToString();
                    pasajero.Apellido = registro["apellido"].ToString();
                    pasajero.Mail = registro["mail"].ToString();
                    pasajero.DNI = int.Parse(registro["dni"].ToString());
                    pasajero.Telefono = int.Parse(registro["telefono"].ToString());
                    pasajero.Activo = int.Parse(registro["activo"].ToString());

                    pasajeros.Add(pasajero);
                }
            }

            acceso.Cerrar();

            return pasajeros;
        }
    }

}