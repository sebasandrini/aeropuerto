﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;

namespace DAL
{
    public class MP_Vuelo
    {
        Acceso acceso = new Acceso();

        MP_Destino gestor_destino = new MP_Destino();
        MP_Avion gestor_avion = new MP_Avion();

        public int InsertarVuelo(BE.Vuelo vuelo)
        {
            List<SqlParameter> parametros = new List<SqlParameter>();

            SqlParameter fechaPartida = new SqlParameter();
            fechaPartida.ParameterName = "@fechaPartida";
            fechaPartida.Value = vuelo.FechaPartida.ToString("yyyy-MM-dd HH:mm");
            fechaPartida.SqlDbType = SqlDbType.Text;
            parametros.Add(fechaPartida);

            SqlParameter fechaLlegada = new SqlParameter();
            fechaLlegada.ParameterName = "@fechaLlegada";
            fechaLlegada.Value = vuelo.FechaLlegada.ToString("yyyy-MM-dd HH:mm");
            fechaLlegada.SqlDbType = SqlDbType.Text;
            parametros.Add(fechaLlegada);

            SqlParameter origen = new SqlParameter();
            origen.ParameterName = "@origen";
            origen.Value = vuelo.Origen.Id;
            origen.SqlDbType = SqlDbType.Int;
            parametros.Add(origen);

            SqlParameter destino = new SqlParameter();
            destino.ParameterName = "@destino";
            destino.Value = vuelo.Destino.Id;
            destino.SqlDbType = SqlDbType.Int;
            parametros.Add(destino);

            SqlParameter avion = new SqlParameter();
            avion.ParameterName = "@avion";
            avion.Value = vuelo.Avion.Id;
            avion.SqlDbType = SqlDbType.Int;
            parametros.Add(avion);

            SqlParameter escalas = new SqlParameter();
            escalas.ParameterName = "@escalas";
            escalas.Value = ParsearEscalas(vuelo);
            escalas.SqlDbType = SqlDbType.Text;
            parametros.Add(escalas);

            SqlParameter tipo = new SqlParameter();
            tipo.ParameterName = "@tipo";
            tipo.Value = vuelo.Tipo;
            tipo.SqlDbType = SqlDbType.Text;
            parametros.Add(tipo);

            string SQL = "InsertarVuelo";

            int resultado = acceso.Escribir(SQL, parametros);

            return resultado;
        }

        public int CancelarVuelo(BE.Vuelo vuelo)
        {
            List<SqlParameter> parametros = new List<SqlParameter>();

            SqlParameter nroVuelo = new SqlParameter();
            nroVuelo.ParameterName = "@nroVuelo";
            nroVuelo.Value = vuelo.NroVuelo;
            nroVuelo.SqlDbType = SqlDbType.Int;
            parametros.Add(nroVuelo);

            string SQL = "CancelarVuelo";

            int resultado = acceso.Escribir(SQL, parametros);

            return resultado;
        }

        public List<BE.Vuelo> ListarVuelos()
        {
            List<BE.Vuelo> vuelos = new List<BE.Vuelo>();

            acceso.Abrir();

            DataTable tabla = acceso.Leer("ListarVuelos");

            int asientosOcupados = 0;

            foreach (DataRow registro in tabla.Rows)
            {
                if (int.Parse(registro["estadoVuelo"].ToString()) != 3)
                {
                    BE.Vuelo vuelo = new BE.Vuelo();

                    vuelo.NroVuelo = int.Parse(registro["nroVuelo"].ToString());
                    vuelo.FechaPartida = DateTime.Parse(registro["fechaPartida"].ToString());
                    vuelo.FechaLlegada = DateTime.Parse(registro["fechaLlegada"].ToString());
                    vuelo.EstadoVuelo = (BE.Enum_EstadoVuelo)Enum.Parse(typeof(BE.Enum_EstadoVuelo),registro["estadoVuelo"].ToString(),true);

                    foreach (BE.Destino d in gestor_destino.ListarDestinos())
                    {
                        if (d.Id == int.Parse(registro["origen"].ToString()))
                        {
                            vuelo.Origen = d;
                        }
                        else if (d.Id == int.Parse(registro["destino"].ToString()))
                        {
                            vuelo.Destino = d;
                        }
                        //else if (registro["escalas"].ToString().Split(';').Contains(d.Id.ToString()))
                        //{
                        //    vuelo.Escalas.Add(d);
                        //}
                    }
                    
                    foreach(BE.Avion a in gestor_avion.ListarAviones())
                    {
                        if (a.Id == int.Parse(registro["avion"].ToString()))
                        {
                            vuelo.Avion = a;
                            break;
                        }
                    }
                    asientosOcupados = int.Parse(registro["ocupacion"].ToString());
                    vuelo.Ocupacion = (asientosOcupados * 100 / vuelo.Avion.Capacidad).ToString();
                    vuelo.Tipo = registro["tipo"].ToString();

                    vuelos.Add(vuelo);
                }
            }

            acceso.Cerrar();

            return vuelos;
        }
    
        string ParsearEscalas(BE.Vuelo vuelo)
        {
            string escalas = "";

            foreach (BE.Destino d in vuelo.Escalas)
            {
                escalas += d.Id.ToString() + ";";
            }

            if (escalas.Length>0)
            {
                escalas = escalas.Remove(escalas.Length - 1);
            }
            

            return escalas;
        }
    }
}