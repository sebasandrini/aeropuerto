﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;

namespace DAL
{
    public class MP_Pasaje
    {
        Acceso acceso = new Acceso();
        MP_Vuelo gestor_vuelo = new MP_Vuelo();
        MP_Pasajero gestor_pasajero = new MP_Pasajero();


        public int InsertarPasaje(BE.Pasaje pasaje)
        {
            List<SqlParameter> parametros = new List<SqlParameter>();

            SqlParameter pasajero = new SqlParameter();
            pasajero.ParameterName = "@pasajero";
            pasajero.Value = pasaje.Pasajero.Id;
            pasajero.SqlDbType = SqlDbType.Int;
            parametros.Add(pasajero);

            SqlParameter nroVuelo = new SqlParameter();
            nroVuelo.ParameterName = "@nroVuelo";
            nroVuelo.Value = pasaje.Vuelo.NroVuelo;
            nroVuelo.SqlDbType = SqlDbType.Int;
            parametros.Add(nroVuelo);

            SqlParameter total = new SqlParameter();
            total.ParameterName = "@total";
            total.Value = pasaje.Total;
            total.SqlDbType = SqlDbType.Money;
            parametros.Add(total);

            string SQL = "InsertarPasaje";

            int resultado = acceso.Escribir(SQL, parametros);

            return resultado;
        }

        public List<BE.Pasaje> ListarPasajes()
        {
            List<BE.Pasaje> pasajes = new List<BE.Pasaje>();

            acceso.Abrir();

            DataTable tabla = acceso.Leer("ListarPasajes");

            foreach (DataRow registro in tabla.Rows)
            {
                BE.Pasaje pasaje = new BE.Pasaje();

                pasaje.Id = int.Parse(registro["id"].ToString());
                pasaje.FechaCompra = DateTime.Parse(registro["fechaCompra"].ToString());
                pasaje.Total = double.Parse(registro["total"].ToString());

                foreach(BE.Pasajero p in gestor_pasajero.ListarPasajeros())
                {
                    if (p.Id == int.Parse(registro["pasajero"].ToString()))
                    {
                        pasaje.Pasajero = p;
                    }
                }
                foreach(BE.Vuelo v in gestor_vuelo.ListarVuelos())
                {
                    if (v.NroVuelo == int.Parse(registro["nroVuelo"].ToString()))
                    {
                        pasaje.Vuelo = v;
                    }
                }

                if(pasaje.Vuelo != null)
                {
                    pasajes.Add(pasaje);
                }
            }

            acceso.Cerrar();

            return pasajes;
        }
    }
}