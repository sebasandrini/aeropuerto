﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;

namespace DAL
{
    public class MP_Destino
    {
        Acceso acceso = new Acceso();

        public int InsertarDestino(BE.Destino destino)
        {
            List<SqlParameter> parametros = new List<SqlParameter>();

            SqlParameter ciudad = new SqlParameter();
            ciudad.ParameterName = "@ciudad";
            ciudad.Value = destino.Ciudad;
            ciudad.SqlDbType = SqlDbType.Text;
            parametros.Add(ciudad);

            SqlParameter pais = new SqlParameter();
            pais.ParameterName = "@pais";
            pais.Value = destino.Pais;
            pais.SqlDbType = SqlDbType.Text;
            parametros.Add(pais);

            SqlParameter aeropuerto = new SqlParameter();
            aeropuerto.ParameterName = "@aeropuerto";
            aeropuerto.Value = destino.Aeropuerto;
            aeropuerto.SqlDbType = SqlDbType.Text;
            parametros.Add(aeropuerto);

            SqlParameter latitud = new SqlParameter();
            latitud.ParameterName = "@latitud";
            latitud.Value = destino.Latitud;
            latitud.SqlDbType = SqlDbType.Text;
            parametros.Add(latitud);

            SqlParameter longitud = new SqlParameter();
            longitud.ParameterName = "@longitud";
            longitud.Value = destino.Longitud;
            longitud.SqlDbType = SqlDbType.Text;
            parametros.Add(longitud);

            string SQL = "InsertarDestino";

            int resultado = acceso.Escribir(SQL, parametros);

            return resultado;
        }

        public int ModificarDestino(BE.Destino destino)
        {
            List<SqlParameter> parametros = new List<SqlParameter>();

            SqlParameter id = new SqlParameter();
            id.ParameterName = "@id";
            id.Value = destino.Id;
            id.SqlDbType = SqlDbType.Int;
            parametros.Add(id);

            SqlParameter ciudad = new SqlParameter();
            ciudad.ParameterName = "@ciudad";
            ciudad.Value = destino.Ciudad;
            ciudad.SqlDbType = SqlDbType.Text;
            parametros.Add(ciudad);

            SqlParameter pais = new SqlParameter();
            pais.ParameterName = "@pais";
            pais.Value = destino.Pais;
            pais.SqlDbType = SqlDbType.Text;
            parametros.Add(pais);

            SqlParameter aeropuerto = new SqlParameter();
            aeropuerto.ParameterName = "@aeropuerto";
            aeropuerto.Value = destino.Aeropuerto;
            aeropuerto.SqlDbType = SqlDbType.Text;
            parametros.Add(aeropuerto);

            SqlParameter latitud = new SqlParameter();
            latitud.ParameterName = "@latitud";
            latitud.Value = destino.Latitud;
            latitud.SqlDbType = SqlDbType.Text;
            parametros.Add(latitud);

            SqlParameter longitud = new SqlParameter();
            longitud.ParameterName = "@longitud";
            longitud.Value = destino.Longitud;
            longitud.SqlDbType = SqlDbType.Text;
            parametros.Add(longitud);

            string SQL = "ModificarDestino";

            int resultado = acceso.Escribir(SQL, parametros);

            return resultado;
        }

        public int EliminarDestino(BE.Destino destino)
        {
            List<SqlParameter> parametros = new List<SqlParameter>();

            SqlParameter id = new SqlParameter();
            id.ParameterName = "@id";
            id.Value = destino.Id;
            id.SqlDbType = SqlDbType.Int;
            parametros.Add(id);

            string SQL = "EliminarDestino";

            int resultado = acceso.Escribir(SQL, parametros);

            return resultado;
        }

        public List<BE.Destino> ListarDestinos()
        {
            List<BE.Destino> destinos = new List<BE.Destino>();

            acceso.Abrir();

            DataTable tabla = acceso.Leer("ListarDestinos");

            foreach (DataRow registro in tabla.Rows)
            {
                if (int.Parse(registro["activo"].ToString()) != 0)
                {
                    BE.Destino destino = new BE.Destino();

                    destino.Id = int.Parse(registro["id"].ToString());
                    destino.Ciudad = registro["ciudad"].ToString();
                    destino.Pais = registro["pais"].ToString();
                    destino.Aeropuerto = registro["aeropuerto"].ToString();
                    destino.Latitud = registro["latitud"].ToString();
                    destino.Longitud = registro["longitud"].ToString();
                    destino.Activo = int.Parse(registro["activo"].ToString());

                    destinos.Add(destino);
                }
            }

            acceso.Cerrar();

            return destinos;
        }
    }
}