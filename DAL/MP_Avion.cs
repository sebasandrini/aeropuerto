﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;

namespace DAL
{
    public class MP_Avion
    {
        Acceso acceso = new Acceso();

        public int InsertarAvion(BE.Avion avion)
        {
            List<SqlParameter> parametros = new List<SqlParameter>();

            SqlParameter tipo = new SqlParameter();
            tipo.ParameterName = "@tipo";
            tipo.Value = avion.Tipo;
            tipo.SqlDbType = SqlDbType.Text;
            parametros.Add(tipo);

            SqlParameter capacidad = new SqlParameter();
            capacidad.ParameterName = "@capacidad";
            capacidad.Value = avion.Capacidad;
            capacidad.SqlDbType = SqlDbType.Int;
            parametros.Add(capacidad);

            string SQL = "InsertarAvion";

            int resultado = acceso.Escribir(SQL, parametros);

            return resultado;
        }

        public int ModificarAvion(BE.Avion avion)
        {
            List<SqlParameter> parametros = new List<SqlParameter>();

            SqlParameter id = new SqlParameter();
            id.ParameterName = "@id";
            id.Value = avion.Id;
            id.SqlDbType = SqlDbType.Int;
            parametros.Add(id);

            SqlParameter tipo = new SqlParameter();
            tipo.ParameterName = "@tipo";
            tipo.Value = avion.Tipo;
            tipo.SqlDbType = SqlDbType.Text;
            parametros.Add(tipo);

            SqlParameter capacidad = new SqlParameter();
            capacidad.ParameterName = "@capacidad";
            capacidad.Value = avion.Capacidad;
            capacidad.SqlDbType = SqlDbType.Int;
            parametros.Add(capacidad);

            string SQL = "ModificarAvion";

            int resultado = acceso.Escribir(SQL, parametros);

            return resultado;
        }

        public int EliminarAvion(BE.Avion avion)
        {
            List<SqlParameter> parametros = new List<SqlParameter>();

            SqlParameter id = new SqlParameter();
            id.ParameterName = "@id";
            id.Value = avion.Id;
            id.SqlDbType = SqlDbType.Int;
            parametros.Add(id);

            string SQL = "EliminarAvion";

            int resultado = acceso.Escribir(SQL, parametros);

            return resultado;
        }

        public List<BE.Avion> ListarAviones()
        {
            List<BE.Avion> aviones = new List<BE.Avion>();

            acceso.Abrir();

            DataTable tabla = acceso.Leer("ListarAviones");

            foreach (DataRow registro in tabla.Rows)
            {
                if (int.Parse(registro["activo"].ToString()) != 0)
                {
                    BE.Avion avion = new BE.Avion();

                    avion.Id = int.Parse(registro["id"].ToString());
                    avion.Tipo = registro["tipo"].ToString();
                    avion.Capacidad = int.Parse(registro["capacidad"].ToString());
                    avion.Activo = int.Parse(registro["activo"].ToString());

                    aviones.Add(avion);
                }
            }

            acceso.Cerrar();

            return aviones;
        }
    }
}