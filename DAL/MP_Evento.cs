﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;

namespace DAL
{
    public class MP_Evento
    {
        Acceso acceso = new Acceso();

        MP_Vuelo gestor_vuelo = new MP_Vuelo();

        public int InsertarEvento(BE.Evento evento)
        {
            List<SqlParameter> parametros = new List<SqlParameter>();

            SqlParameter fechaEvento = new SqlParameter();
            fechaEvento.ParameterName = "@fechaEvento";
            fechaEvento.Value = evento.FechaEvento.ToString("yyyy-MM-dd HH:mm");
            fechaEvento.SqlDbType = SqlDbType.Text;
            parametros.Add(fechaEvento);

            SqlParameter nroVuelo = new SqlParameter();
            nroVuelo.ParameterName = "@nroVuelo";
            nroVuelo.Value = evento.Vuelo.NroVuelo;
            nroVuelo.SqlDbType = SqlDbType.Int;
            parametros.Add(nroVuelo);

            SqlParameter tipoEvento = new SqlParameter();
            tipoEvento.ParameterName = "@tipoEvento";
            tipoEvento.Value = evento.TipoEvento;
            tipoEvento.SqlDbType = SqlDbType.Text;
            parametros.Add(tipoEvento);

            SqlParameter estadoVuelo = new SqlParameter();
            estadoVuelo.ParameterName = "@estadoVuelo";
            estadoVuelo.Value = (int)((BE.Enum_EstadoVuelo)evento.Vuelo.EstadoVuelo);
            estadoVuelo.SqlDbType = SqlDbType.Int;
            parametros.Add(estadoVuelo);

            string SQL = "InsertarEvento";

            int resultado = acceso.Escribir(SQL, parametros);

            return resultado;
        }

        public List<BE.Evento> ListarEventos()
        {
        List<BE.Evento> eventos = new List<BE.Evento>();

        acceso.Abrir();

        DataTable tabla = acceso.Leer("ListarEventos");

        foreach (DataRow registro in tabla.Rows)
        {
                BE.Evento evento = new BE.Evento();

                evento.FechaEvento = DateTime.Parse(registro["fechaEvento"].ToString());
                evento.TipoEvento = registro["tipoEvento"].ToString();

                foreach (BE.Vuelo v in gestor_vuelo.ListarVuelos())
                {
                    if (v.NroVuelo == int.Parse(registro["nroVuelo"].ToString()))
                    {
                        evento.Vuelo = v;
                    }
                }

                eventos.Add(evento);
            }

            acceso.Cerrar();

            return eventos;
        }
    }
}